//sales by bill no
$(".psbillno").autocomplete({
    source: 'psbill/no',
    minLength: 1,
    select: function(event, ui)
    {
      value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type:'get',
            url:'psbill/details',
            data:{value:value},
            success:function(data)
            {
                var json = $.parseJSON(data);
                // alert(json['custname']);
                $('#pcustname').val(json['custname']);                
                $('#pcustid').val(json['custid']);
                $('#psaletot').val(json['total']);
                var amount = parseFloat(json['amountpay'] + json['amountpayl']);
                $('#psalespayed').val(amount);
                if(amount == json['total']){
                    $('#psalespay').attr('readonly', true);
                } else {
                    $('#psalespay').attr('readonly', false);
                }
            }
        })
    }
});

//sales by cust num
$(".pcustname").autocomplete({
    source: 'customer/custname',
    minLength: 1,
    select: function(event, ui)
    {
      value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type:'get',
            url:'customer/details',
            data:{value:value},
            success:function(data)
            {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $.ajax({
                    type:'get',
                    url:'cust/billno',
                    data:{value:json['id']},
                    success:function(data)
                    {
                        var json = $.parseJSON(data);
                        // alert(json.length);
                        $('#psbillno').children('option').remove();
                        $('#psbillno').append($("<option></option>")
                                        .text('--Select--'));
                        for(var i = 0; i < json.length; i++){
                            $('#psbillno').append($("<option></option>")
                                        .attr("value",json[i])
                                        .text(json[i]));
                        }
                    }
                })

            }
        })
    }
});

$('#psbillno').change(function() {
    var billno = $('#psbillno').val();
    $.ajax({
        type:'get',
        url:'psbill/details',
        data:{value:billno},
        success:function(data)
        {
            var json = $.parseJSON(data);
            // alert(json['custname']);
            // $('#pcustname').val(json['custname']);                
            // $('#pcustid').val(json['custid']);
            $('#psaletot').val(json['total']);
            var amount = parseFloat(json['amountpay'] + json['amountpayl']);
            $('#psalespayed').val(amount);
            if(amount == json['total']){
                $('#psalespay').attr('readonly', true);
            } else {
                $('#psalespay').attr('readonly', false);
            }
        }
    })

});


//purchase by bill no
$(".ppbillno").autocomplete({
    source: 'ppbill/no',
    minLength: 1,
    select: function(event, ui)
    {
      value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type:'get',
            url:'ppbill/details',
            data:{value:value},
            success:function(data)
            {
                var json = $.parseJSON(data);
                // alert(json['ppurname']);
                $('#ppurname').val(json['ppurname']);                
                $('#ppurid').val(json['ppurid']);
                $('#ppurchasetot').val(json['ppurchasetot']);
                var amount = parseFloat(json['amountpay'] + json['amountpayl']);
                $('#ppurchasepayed').val(amount);
                if(amount == json['ppurchasetot']){
                    $('#ppurchasepay').attr('readonly', true);
                } else {
                    $('#ppurchasepay').attr('readonly', false);
                }
            }
        })
    }
});

//purchase by name
$(".ppurname").autocomplete({
    source: 'purchaser/purname',
    minLength: 1,
    select: function(event, ui)
    {
      value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type:'get',
            url:'purchaser/details',
            data:{value:value},
            success:function(data)
            {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $.ajax({
                    type:'get',
                    url:'purc/billno',
                    data:{value:json['id']},
                    success:function(data)
                    {
                        var json = $.parseJSON(data);
                        // alert(json.length);
                        $('#ppbillno').children('option').remove();
                        $('#ppbillno').append($("<option></option>")
                                        .text('--Select--'));
                        for(var i = 0; i < json.length; i++){
                            $('#ppbillno').append($("<option></option>")
                                        .attr("value",json[i])
                                        .text(json[i]));
                        }
                    }
                })

            }
        })
    }
});

$('#ppbillno').change(function() {
    var billno = $('#ppbillno').val();
    $.ajax({
        type:'get',
        url:'ppbill/details',
        data:{value:billno},
        success:function(data)
        {
            var json = $.parseJSON(data);
            // alert(json['ppurname']);
            // $('#ppurname').val(json['ppurname']);                
            // $('#ppurid').val(json['ppurid']);
            $('#ppurchasetot').val(json['ppurchasetot']);
            var amount = parseFloat(json['amountpay'] + json['amountpayl']);
            $('#ppurchasepayed').val(amount);
            if(amount == json['ppurchasetot']){
                $('#ppurchasepay').attr('readonly', true);
            } else {
                $('#ppurchasepay').attr('readonly', false);
            }
        }
    })

});

//submission conditions.Sales

$('#psalespay').on('input',function() {
    var psalespay = $('#psalespay').val();
    var amountpayed = $('#psalespayed').val(); 
    var total = $('#psaletot').val(); 
    var bal = parseFloat(total) - parseFloat(amountpayed);
    if(psalespay > bal){
        // $('#psalespay').attr('readonly', true);
        $('#psalespay').css({"border-color":"red"});
        $('#newsubmitsales').addClass('display-hide');
        $('#alertsalespa').removeClass('display-hide');
    } else {        
        $('#psalespay').css({"border-color":""});
        $('#newsubmitsales').removeClass('display-hide');
        $('#alertsalespa').addClass('display-hide');
    }

});

//submission conditions.purchase

$('#ppurchasepay').on('input',function() {
    var ppurchasepay = $('#ppurchasepay').val();
    var amountpayed = $('#ppurchasepayed').val(); 
    var total = $('#ppurchasetot').val(); 
    var bal = parseFloat(total) - parseFloat(amountpayed);
    if(ppurchasepay > bal){
        // $('#psalespay').attr('readonly', true);
        $('#ppurchasepay').css({"border-color":"red"});
        $('#newsubmitpur').addClass('display-hide');
        $('#alertpurpa').removeClass('display-hide');
    } else {        
        $('#ppurchasepay').css({"border-color":""});
        $('#newsubmitpur').removeClass('display-hide');
        $('#alertpurpa').addClass('display-hide');
    }

});
