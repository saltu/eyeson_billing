$(".custname").autocomplete({
    source: 'customer/custname',
    minLength: 1,
    select: function(event, ui)
    {
      value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type:'get',
            url:'customer/details',
            data:{value:value},
            success:function(data)
            {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#custid').val(json['id']);
                $('#phone').val(json['phone']);
            }
        })
    }
});

$(".typeaheadest").autocomplete({
    source: 'product/prodname',
    minLength: 1,
    select: function(event, ui)
    {
      value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type:'get',
            url:'product/details',
            data:{value:value},
            success:function(data)
            {
                $('#item-qtyest').attr("readonly", false);
                $('#item-rateest').attr("readonly", false);
//                $('#prodid').attr("readonly", true);
                var json = $.parseJSON(data);
                $('#prodid').val(json['id']);
                $('#item-codeest').val(json['code']);
                var rateo = parseFloat(json['rate']) * parseFloat((json['gst'])/100);
                var tt = parseFloat(rateo + json['rate']);
                $('#item-rateoest').val(tt);
            }
        })
    }
});


$('#phone').on('input',function(e){
    var maxLen = 10;
    var Length = $("#phone").val().length;
    if(Length > maxLen){
            var phone = $("#phone").val();
            $('#phone').css({"border-color":"red"});
            $('#alert').removeClass('display-hide');
            $('#alert').html("Please enter a valid Phone number");
            phone = phone.slice(0,-1);
            $('#phone').val(phone);
    }
});

$('#item-rateest').on('input',function(e){

    var qty1 = $('#item-qtyest').val();
    var rate = $('#item-rateest').val();
    
    var tot = $('#item-totallest').val();
    
    var subtotal = $('#subtotal').val();
    var subtotaltem = $('#subtotaltem').val();

    var amo_p = parseFloat(rate);
    $('#item-ratepest').val(Math.round(amo_p));
    
    if (qty1 == '')
        {
           console.log("null");
           if (tot != '0')
               {
                   $('#subtotal').val(subtotaltem); 
               }
        }
    else
        {
            var am_tot = parseFloat(rate * qty1); 
            var va3 = parseFloat(subtotaltem) + parseFloat(am_tot);
            $('#item-totallest').val(Math.round(am_tot));
            $('#subtotal').val(Math.round(va3));
         }
});

$('#item-qtyest').on('input',function(e){
    
    var qty1 = parseFloat($('#item-qtyest').val());
    var rate = $('#item-rateest').val();
    
    var subtotal = $('#subtotal').val();

    var subtotaltem = $('#subtotaltem').val();
    
    $('#item-totallest').val(rate);
    
    if (qty1 == '' || qty1 == '0' )
    {
           console.log("null");
           $('#subtotal').val(subtotaltem); 
           $('#item-qtyest').val(""); 
    }
    else
    {
            var am_tot = parseFloat(rate * qty1); 
            var va3 = parseFloat(subtotaltem) + parseFloat(am_tot);
            $('#item-totallest').val(Math.round(am_tot));
            $('#subtotal').val(Math.round(va3));        
    }
});


//on submit validation of product entery
$('.submitsses').on('click',function(e){
    var item = $('#typeaheadest').val();
    var item_rate = $('#item-rateest').val();
    var item_qty = $('#item-qtyest').val();
    
    if(item != ''){
        $('#typeaheadest').css({"border-color":"red"});       
        $('#alert25').removeClass('display-hide');        
        $('#typeaheadest').focus();
        return false;
    }  else if(item_rate != ''){
        $('#typeaheadest').css({"border-color":""});              
        $('#alert25').addClass('display-hide');
        $('#item-rateest').focus();
        $('#item-rateest').css({"border-color":"red"});      
        $('#alert25').removeClass('display-hide');   
        return false;
    } else if(item_qty != ''){
        $('#item-rateest').css({"border-color":""});            
        $('#alert25').addClass('display-hide');
        $('#item-qtyest').focus();
        $('#item-qtyest').css({"border-color":"red"});      
        $('#alert25').removeClass('display-hide');   
        return false;
    } else { 
        $('#item-qtyest').css({"border-color":""});            
        $('#alert25').addClass('display-hide');
        return true;
    }
});


var TableDatatablesEditable_sales_est = function () {

    var handleTable = function () {

        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);

            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }

            oTable.fnDraw();
        }

        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);          
            jqTds[0].innerHTML = '';
            jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
            jqTds[2].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[2] + '">';
            jqTds[3].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[3] + '">';
            jqTds[4].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[4] + '">';
            jqTds[5].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[5] + '">';
            jqTds[6].innerHTML = '<a class="cancelsales" href="">Cancel</a>';
        }

        function saveRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
            oTable.fnUpdate(jqInputs[3].value, nRow, 4, false);
            oTable.fnUpdate(jqInputs[4].value, nRow, 5, false);
            oTable.fnUpdate(jqInputs[5].value, nRow, 6, false);
            oTable.fnUpdate('<a class="deletesales" href="">Delete</a>', nRow, 7, false);
            oTable.fnDraw();
        }

        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
            oTable.fnUpdate(jqInputs[3].value, nRow, 4, false);
            oTable.fnUpdate(jqInputs[4].value, nRow, 5, false);
            oTable.fnUpdate(jqInputs[5].value, nRow, 6, false);
            oTable.fnUpdate('<a class="editsales" href="">Edit</a>', nRow, 7, false);
            oTable.fnDraw();
        }

        var table = $('#sample_editable_1_sales_est');

        var oTable = table.dataTable({

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // set the initial value
            "pageLength": 5,

            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "columnDefs": [{ // set default column settings
                'orderable': true,
                'targets': [0]
            }, {
                "searchable": true,
                "targets": [0]
            }],
            "order": [
                [0, "asc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = $("#sample_editable_1_wrapper_sales_est");

        var nEditing = null;
        var nNew = false;

        $('#sample_editable_1_new_sales_est').click(function (e) {
            e.preventDefault();

            var item = $('#typeaheadest').val();
            var item_code = $('#item-codeest').val();
            var item_rate = $('#item-rateest').val();
            var item_qty = $('#item-qtyest').val();
            var item_total = $('#item-totallest').val();
            var custname = $('#custname').val();
            var custphone = $('#phone').val();            
            var custid = $('#custid').val();            
            var prodid = $('#item-codeest').val();         
            var cudate = $('#cudate').val();
            
            var qty1 = parseFloat($('#item-qtyest').val());
            // alert (custname+"jj"+custphone);
            console.log(item)
            if (nNew && nEditing) {
               
                $('#alert2').removeClass("display-hide");
                $('#alert2').html("Previous row not saved. Reloading");
                var delay = 1000; 
                // oTable.fnDeleteRow(nEditing);
                // nEditing = null;
                // nNew = false;
                setTimeout(function(){ window.location = 'salesadd'; }, delay);
                // if (confirm("Previose row not saved. Do you want to save it ?")) {
                //     saveRow(oTable, nEditing); // save
                //     $(nEditing).find("td:first").html("Untitled");
                //     nEditing = null;
                //     nNew = false;

                // } else {
                //     oTable.fnDeleteRow(nEditing); // cancel
                //     nEditing = null;
                //     nNew = false;
                    
                //     return;
                // }
            }

            if(cudate == ''){
                $('#cudate').css({"border-color":"red"});
                $('#cudate').focus();
            } else if(item == ''){
                $('#cudate').css({"border-color":""});
                $('#typeaheadest').css({"border-color":"red"});
                $('#typeaheadest').focus();
            }  else if(item_rate == ''){
                $('#typeaheadest').css({"border-color":""});
                $('#item-rateest').focus();
                $('#item-rateest').css({"border-color":"red"});
            } else if(item_qty == ''){
                $('#item-rateest').css({"border-color":""});
                $('#item-qtyest').focus();
                $('#item-qtyest').css({"border-color":"red"});
            }  else if(custname == ''){
                $('#item-qtyest').css({"border-color":""});
                $('#custname').focus();
                $('#custname').css({"border-color":"red"});
            }  else if(custphone == ''){
                $('#custname').css({"border-color":""});
                $('#phone').focus();
                $('#phone').css({"border-color":"red"});
            } else { 
                $('#item-qtyest').css({"border-color":""});
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                   type:'POST',
                   url:'temp/add',
                   data: {
                        cudate: cudate,
                        custname: custname,
                        custphone: custphone,
                        prodname: item,
                        prodid: item_code,
                        item_rate: item_rate,
                        item_qty: item_qty,
                        item_total: item_total
                    },
                   success:function(data) {

                      window.location = 'sales';
                   }
                });
                var aiNew = oTable.fnAddData(['', item, item_code, item_rate, item_qty, item_total ,'<a class="delete" href="">Delete</a>' ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                // savedataRow(oTable, nRow);
                // $('#sales-table')[0].reset();
                nEditing = nRow;
                nNew = true;
            }            
        });

        table.on('click', '.deletesales', function (e) {
            e.preventDefault();

            if (confirm("Are you sure to delete this Item ?") == false) {
                return;
            }

            var nRow = $(this).parents('tr').attr('id');
            // alert(nRow);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
               type:'POST',
               url:'temp/delete/data',
               data: { id: nRow },
               success:function(data) {
                  $('#alert2').html(data.success);
                  window.location = 'sales';
               }
            });
            oTable.fnDeleteRow(nRow);
            // alert("Deleted! Do not forget to do some ajax to sync with backend :)");

        });

        table.on('click', '.cancelsales', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        // table.on('click', '.editsales', function (e) {
        //     e.preventDefault();
        //     nNew = false;
            
        //     /* Get the row as a parent of the link that was clicked on */
        //     var nRow = $(this).parents('tr')[0];

        //     if (nEditing !== null && nEditing != nRow) {
        //         /* Currently editing - but not this row - restore the old before continuing to edit mode */
        //         // $('#sample_editable_1_new_sales').addClass('display-hide')
        //         restoreRow(oTable, nEditing);
        //         editRow(oTable, nRow);
        //         nEditing = nRow;
        //         alert("backend :)");
        //     } else if (nEditing == nRow) {
        //         /* Editing this row and want to save it */
        //         saveRow(oTable, nEditing);
        //         nEditing = null;
        //         alert("Updated! Do not forget to do some ajax to sync with backend :)");
        //     } else {
        //         /* No edit in progress - let's start one */

        //         // $('#sample_editable_1_new_sales').addClass('display-hide')
        //         editRow(oTable, nRow);
        //         nEditing = nRow;
        //         alert("Updated:)");
        //     }
        // });
    }

    return {

        //main function to initiate the module
        init: function () {
            handleTable();
        }

    };

}();

jQuery(document).ready(function() {
    TableDatatablesEditable_sales_est.init();
});