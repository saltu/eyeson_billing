$('#pdatefrom').datepicker({
    changeMonth:true,
    changeMonth:true,
    changeYear:true,
    yearRange:"1970:+0",
    dateFormat:"yy/mm/dd",
    onSelect:function(dateText){
        var datefrom = $('#pdatefrom').val();
        var dateto = $('#pdateto').val();
        if(dateto == ''){
            // $('#pdatefrom').focus();
            $('#pdateto').css({"border-color":"red"});
        } else {
            $('#pdateto').css({"border-color":""});
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
               type:'POST',
               url:'purchasereport/data',
               data: { 
                    datefrom: datefrom,
                    dateto: dateto
               },
               success:function(data) {
                    // alert(data);
                  $('#purchasereport1').removeClass("display-hide");
                  $('#purchasereport2').removeClass("display-hide");
                  $('.purchasereport2').html(data);
               }
            });
        }        
    }
});
$('#pdateto').datepicker({
    changeMonth:true,
    changeMonth:true,
    changeYear:true,
    yearRange:"1970:+0",
    dateFormat:"yy/mm/dd",
    onSelect:function(dateText){
        $('#pdateto').css({"border-color":""});
        var datefrom = $('#pdatefrom').val();
        var dateto = $('#pdateto').val();
        if(datefrom == ''){
            // $('#sdatefrom').focus();
            $('#pdatefrom').css({"border-color":"red"});
            $('#pdateto').val('');
        } else {
            $('#pdatefrom').css({"border-color":""});
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
               type:'POST',
               url:'purchasereport/data',
               dataSrc: "",
               data: { 
                    datefrom: datefrom,
                    dateto: dateto
               },
               success:function(data) {
                  $('#purchasereport1').removeClass("display-hide");
                  $('#purchasereport2').removeClass("display-hide");
                  $('.purchasereport2').html(data);
               }
            });
        }        
    }
});

//purchasereport
var TableDatatablesEditable_purchasereport = function () {

    var handleTable = function () {

        var table = $('#sample_editable_1_purchasereport');

        var oTable = table.dataTable({

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
            // "sScrollX": "90%",
            // "sScrollXInner": "100%",
            // "bScrollCollapse": true,
            "dom": 'Bfrtip',
            "buttons": [
               {
                    "text": '<i class="fa fa-lg fa-file-excel-o"></i>',
                    "extend": 'excel',
                    "className": 'btn btn-xs btn-primary p-5 m-0 width-35 assets-export-btn export-xls ttip',
                    "title": 'EyesOn Product Details',
                    "extension": '.xls'
                }, {
                    "text": '<i class="fa fa-lg fa-file-pdf-o"></i>',
                    "extend": 'pdf',
                    "className": 'btn btn-xs btn-primary p-5 m-0 width-35 assets-export-btn export-pdf ttip',
                    "title": 'EyesOn Product Details',
                    "extension": '.pdf'
                }, {
                    "text": '<i class="fa fa-lg fa-print"></i>',
                    "extend": 'print',
                    "className": 'btn btn-xs btn-primary p-5 m-0 width-35 assets-export-btn export-pdf ttip',
                     "title": 'EyesOn Product Details'
                }
            ],

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // set the initial value
            "pageLength": 5,

            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "columnDefs": [{ // set default column settings
                'orderable': true,
                'targets': [0]
            }, {
                "searchable": true,
                "targets": [0]
            }],
            "order": [
                [0, "asc"]
            ] // set first column as a default sort by asc
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleTable();
        }

    };

}();

jQuery(document).ready(function() {
    TableDatatablesEditable_purchasereport.init();
});