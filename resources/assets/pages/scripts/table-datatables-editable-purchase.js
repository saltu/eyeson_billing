$(".purname").autocomplete({
    source: 'purchaser/purname',
    minLength: 1,
    select: function(event, ui)
    {
      value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type:'get',
            url:'purchaser/details',
            data:{value:value},
            success:function(data)
            {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#purid').val(json['id']);
                $('#phone').val(json['phone']);
                $('#gst').val(json['gst']);
            }
        })
    }
});

$('#pur_discount').on('input',function(e){
    var dis = $('#pur_discount').val();
    var subtotaltem = $('#pursubtotaltem').val();
    var subtotal = $('#pursubtotal').val();
    var tab_total = $('#itempu-totall').val();
    if (subtotal > subtotaltem)
    {
        var amo = parseFloat(subtotaltem) + parseFloat(tab_total) - parseFloat(dis);
        $('#pursubtotal').val(amo);           
    }
    else
    {
        var amo = parseFloat(subtotaltem - dis);
        $('#pursubtotal').val(amo);
    }
});

 $('#itempu-rate').on('input',function(e){
    var qty = $('#itempu-qty').val();
    var rate = $('#itempu-rate').val();
    
    var cgst = $('#itempu-cgstp').val();
    var dis = $('#pur_discount').val();
    
    var subcgst = $('#pursubcgst').val();
    var subsgst = $('#pursubsgst').val();
    var subtotal = $('#pursubtotal').val();
    
    var subcgsttem = $('#pursubcgsttem').val();
    var subsgsttem = $('#pursubsgsttem').val();
    var subtotaltem = $('#pursubtotaltem').val();
     
    if (rate != '' && qty != '' && cgst != '' )
        {
            var am = parseFloat(rate * qty);

            $('#itempu-cgstp').val(cgst);
            $('#itempu-sgstp').val(cgst);

            var cgst1 = parseFloat((am * cgst)/100);
//            var cess = parseFloat((am * 1)/100);
            var sgst1 = parseFloat(cgst1);
            var gst = parseFloat(cgst1 + sgst1)
            $('#itempu-cgst').val(cgst1);
            $('#itempu-sgst').val(sgst1);
//            $('#itempu-cess').val(cess);

            var amo = parseFloat(am + gst);
            console.log(am);
            console.log(cgst);
            console.log(dis1);
            $('#itempu-totall').val(amo);

            var va1 = parseFloat(subcgsttem) + parseFloat(cgst1);
            $('#pursubcgst').val(va1);
            var va2 = parseFloat(subsgsttem) + parseFloat(sgst1);
            $('#pursubsgst').val(va2);
            var va3 = parseFloat(subtotaltem) + parseFloat(amo) - parseFloat(dis);
            $('#pursubtotal').val(va3);
        }
    else
         {
             console.log("rate");
         }
 });


 $('#itempu-qty').on('input',function(e){
    var qty = $('#itempu-qty').val();
    var rate = $('#itempu-rate').val();
    
    var cgst = $('#itempu-cgstp').val();
    var dis = $('#pur_discount').val();
    
    
    var subcgst = $('#pursubcgst').val();
    var subsgst = $('#pursubsgst').val();
    var subtotal = $('#pursubtotal').val();
    
    var subcgsttem = $('#pursubcgsttem').val();
    var subsgsttem = $('#pursubsgsttem').val();
    var subtotaltem = $('#pursubtotaltem').val();
     
    if (qty != '' && rate != '' && cgst != '' )
        {
            var am = parseFloat(rate * qty);

            $('#itempu-cgstp').val(cgst);
            $('#itempu-sgstp').val(cgst);

//            var cess = parseFloat((am * 1)/100);
            var cgst1 = parseFloat((am * cgst)/100);
            var sgst1 = parseFloat(cgst1);
            var gst = parseFloat(cgst1 + sgst1)
            $('#itempu-cgst').val(cgst1);
            $('#itempu-sgst').val(sgst1);
//            $('#itempu-cess').val(cess);

            var amo = parseFloat(am + gst);
            console.log(am);
            console.log(cgst);
            $('#itempu-totall').val(amo);

            var va1 = parseFloat(subcgsttem) + parseFloat(cgst1);
            $('#pursubcgst').val(va1);
            var va2 = parseFloat(subsgsttem) + parseFloat(sgst1);
            $('#pursubsgst').val(va2);
            var va3 = parseFloat(subtotaltem) + parseFloat(amo) - parseFloat(dis);
            $('#pursubtotal').val(va3);
        }
     else
         {
             console.log("qty");
         }
 });

$('#itempu-cgstp').on('input',function(e){
    var qty = $('#itempu-qty').val();
    var rate = $('#itempu-rate').val();
    
    var cgst = $('#itempu-cgstp').val();
    var dis = $('#pur_discount').val();
    
    
    var subcgst = $('#pursubcgst').val();
    var subsgst = $('#pursubsgst').val();
    var subtotal = $('#pursubtotal').val();
    
    var subcgsttem = $('#pursubcgsttem').val();
    var subsgsttem = $('#pursubsgsttem').val();
    var subtotaltem = $('#pursubtotaltem').val();
    // if(qty1 > qty2) {
    //     $('#itempu-qty').val(1);
    //     // alert("Quantity is larger.");
    // }

    if(cgst != ''&& rate != '' && qty != '' )
       {
            var am = parseFloat(rate * qty);

            $('#itempu-cgstp').val(cgst);
            $('#itempu-sgstp').val(cgst);

//            var cess = parseFloat((am * 1)/100);
            var cgst1 = parseFloat((am * cgst)/100);
            var sgst1 = parseFloat(cgst1);
            var gst = parseFloat(cgst1 + sgst1)
            $('#itempu-cgst').val(cgst1);
            $('#itempu-sgst').val(sgst1);
//            $('#itempu-cess').val(cess);

            var amo = parseFloat(am + gst);
            console.log(am);
            console.log(cgst);
            $('#itempu-totall').val(amo);

            var va1 = parseFloat(subcgsttem) + parseFloat(cgst1);
            $('#pursubcgst').val(va1);
            var va2 = parseFloat(subsgsttem) + parseFloat(sgst1);
            $('#pursubsgst').val(va2);
            var va3 = parseFloat(subtotaltem) + parseFloat(amo) - parseFloat(dis);
            $('#pursubtotal').val(va3);
      } 
    else
         {
             console.log("cgstp");
         }
});

$('.submitq').on('click',function(e){
    var item = $('#item_name').val();
    var ime_1 = $('#ime_1').val();
    var item_code = $('#itempu-code').val();
    var item_rate = $('#itempu-rate').val();
    var item_qty = $('#itempu-qty').val();
    var item_cgstp = $('#itempu-cgstp').val();
    var item_sgstp = $('#itempu-sgstp').val();
    var item_cgst = $('#itempu-cgst').val();
    var item_sgst = $('#itempu-sgst').val();
    
    if(item != ''){
        $('#item_name').css({"border-color":"red"});       
        $('#alert25').removeClass('display-hide');        
        $('#item_name').focus();
        return false;
    }  else if(ime_1 != ''){
        $('#item_name').css({"border-color":""});              
        $('#alert25').addClass('display-hide');
        $('#ime_1').focus();
        $('#ime_1').css({"border-color":"red"});      
        $('#alert25').removeClass('display-hide');   
        return false;
    } else if(item_code != ''){
        $('#ime_1').css({"border-color":""});            
        $('#alert25').addClass('display-hide');
        $('#itempu-code').focus();
        $('#itempu-code').css({"border-color":"red"});      
        $('#alert25').removeClass('display-hide');   
        return false;
    } else if(item_rate != ''){
        $('#itempu-code').css({"border-color":""});            
        $('#alert25').addClass('display-hide');
        $('#itempu-rate').focus();
        $('#itempu-rate').css({"border-color":"red"});      
        $('#alert25').removeClass('display-hide');   
        return false;
    } else if(item_qty != ''){
        $('#itempu-rate').css({"border-color":""});            
        $('#alert25').addClass('display-hide');
        $('#itempu-qty').focus();
        $('#itempu-qty').css({"border-color":"red"});
        $('#alert25').removeClass('display-hide');   
        return false;
    } else if(item_cgstp != ''){
        $('#itempu-qty').css({"border-color":""});            
        $('#alert25').addClass('display-hide');
        $('#itempu-cgstp').focus();
        $('#itempu-cgstp').css({"border-color":"red"});      
        $('#alert25').removeClass('display-hide');   
        return false;
    } else { 
        $('#itempu-cgstp').css({"border-color":""});
        $('#alert25').addClass('display-hide');
        return true;
    }
});

//mode rechange
$('#pmode1').on('click',function(e){
    $('#pref').attr('disabled', true);
    $('#pref').addClass('display-hide'); 
    $('.prefdiv').addClass('display-hide');   
});

$('#pmode2').on('click',function(e){
    $('#pref').attr('disabled', false);
    $('#pref').removeClass('display-hide');
    $('.prefdiv').removeClass('display-hide');
});

var TableDatatablesEditable_purchase = function () {

    var handleTable = function () {

        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);

            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }

            oTable.fnDraw();
        }

        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);          
            jqTds[0].innerHTML = '';
            jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
            jqTds[2].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[2] + '">';
            jqTds[3].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[3] + '">';
            jqTds[3].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[4] + '">';
            jqTds[4].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[5] + '">';
            jqTds[5].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[6] + '">';
            jqTds[6].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[7] + '">';
            jqTds[7].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[8] + '">';
            jqTds[8].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[9] + '">';
            jqTds[9].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[10] + '">';
            jqTds[10].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[11] + '">';
            jqTds[11].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[12] + '">';
            jqTds[12].innerHTML = '<a class="cancelpurchase" href="">Cancel</a>';
        }

        function saveRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
            oTable.fnUpdate(jqInputs[3].value, nRow, 4, false);
            oTable.fnUpdate(jqInputs[4].value, nRow, 5, false);
            oTable.fnUpdate(jqInputs[5].value, nRow, 6, false);
            oTable.fnUpdate(jqInputs[6].value, nRow, 7, false);
            oTable.fnUpdate(jqInputs[7].value, nRow, 8, false);
            oTable.fnUpdate(jqInputs[8].value, nRow, 9, false);
            oTable.fnUpdate(jqInputs[9].value, nRow, 10, false);
            oTable.fnUpdate(jqInputs[10].value, nRow, 11, false);
            oTable.fnUpdate(jqInputs[11].value, nRow, 12, false);
            oTable.fnUpdate('<a class="deletepurchase" href="">Delete</a>', nRow, 13, false);
            oTable.fnDraw();
        }

        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 3, false);
            oTable.fnUpdate(jqInputs[3].value, nRow, 4, false);
            oTable.fnUpdate(jqInputs[4].value, nRow, 5, false);
            oTable.fnUpdate(jqInputs[5].value, nRow, 6, false);
            oTable.fnUpdate(jqInputs[6].value, nRow, 7, false);
            oTable.fnUpdate(jqInputs[7].value, nRow, 8, false);
            oTable.fnUpdate(jqInputs[8].value, nRow, 9, false);
            oTable.fnUpdate(jqInputs[9].value, nRow, 10, false);
            oTable.fnUpdate(jqInputs[10].value, nRow, 11, false);
            oTable.fnUpdate(jqInputs[11].value, nRow, 12, false);
            oTable.fnUpdate('<a class="editpurchase" href="">Edit</a>', nRow, 13, false);
            oTable.fnDraw();
        }

        var table = $('#sample_editable_1_purchase');

        var oTable = table.dataTable({

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // set the initial value
            "pageLength": 5,

            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "columnDefs": [{ // set default column settings
                'orderable': true,
                'targets': [0]
            }, {
                "searchable": true,
                "targets": [0]
            }],
            "order": [
                [0, "asc"]
            ] // set first column as a default sort by asc
        });

        var tableWrapper = $("#sample_editable_1_wrapper_purchase");

        var nEditing = null;
        var nNew = false;

        $('#sample_editable_1_new_purchase').click(function (e) {
            e.preventDefault();

            var item = $('#item_name').val();
            var ime_1 = $('#ime_1').val();
            var ime_2 = $('#ime_2').val();
            var item_code = $('#itempu-code').val();
            var item_rate = $('#itempu-rate').val();
            var item_qty = $('#itempu-qty').val();
            var item_dis = $('#pur_discount').val();
            var item_cgstp = $('#itempu-cgstp').val();
            var item_sgstp = $('#itempu-sgstp').val();
            var item_cgst = $('#itempu-cgst').val();
            var item_sgst = $('#itempu-sgst').val();
            var item_total = $('#itempu-totall').val();
            var purname = $('#purname').val();
            var purphone = $('#phone').val();            
            var purid = $('#purid').val();            
            var billno = $('#billno').val();         
            var purbillno = $('#purbillno').val(); 
            var purbilldate = $('#purbilldate').val();
            var gst = $('#gst').val();      
            var pudate = $('#pudate').val();
            // alert (purname+"jj"+purphone);
            
            if (nNew && nEditing) {
               
                $('#alert2').removeClass("display-hide");
                $('#alert2').html("Previous row not saved. Reloading");
                var delay = 1000; 
                // oTable.fnDeleteRow(nEditing);
                // nEditing = null;
                // nNew = false;
                setTimeout(function(){ window.location = 'salesadd'; }, delay);
                // if (confirm("Previose row not saved. Do you want to save it ?")) {
                //     saveRow(oTable, nEditing); // save
                //     $(nEditing).find("td:first").html("Untitled");
                //     nEditing = null;
                //     nNew = false;

                // } else {
                //     oTable.fnDeleteRow(nEditing); // cancel
                //     nEditing = null;
                //     nNew = false;
                    
                //     return;
                // }
            }

            if(pudate == ''){
                $('#pudate').css({"border-color":"red"});
                $('#pudate').focus();
            } else if(item == ''){
                $('#pudate').css({"border-color":""});
                $('#item_name').css({"border-color":"red"});
                $('#item_name').focus();
            }  else if(ime_1 == ''){
                $('#item_name').css({"border-color":""});
                $('#ime_1').focus();
                $('#ime_1').css({"border-color":"red"});
            } else if(item_code == ''){
                $('#ime_1').css({"border-color":""});
                $('#itempu-code').focus();
                $('#itempu-code').css({"border-color":"red"});
            } else if(item_rate == ''){
                $('#itempu-code').css({"border-color":""});
                $('#itempu-rate').focus();
                $('#itempu-rate').css({"border-color":"red"});
            } else if(item_qty == ''){
                $('#itempu-rate').css({"border-color":""});
                $('#itempu-qty').focus();
                $('#itempu-qty').css({"border-color":"red"});
            } else if(item_cgstp == ''){
                $('#itempu-qty').css({"border-color":""});
                $('#itempu-cgstp').focus();
                $('#itempu-cgstp').css({"border-color":"red"});
            } else if(item_sgstp == ''){
                $('#itempu-cgstp').css({"border-color":""});
                $('#itempu-sgstp').focus();
                $('#itempu-sgstp').css({"border-color":"red"});
            } else if(purname == ''){
                $('#itempu-sgstp').css({"border-color":""});
                $('#purname').focus();
                $('#purname').css({"border-color":"red"});
            } else if(purphone == ''){
                $('#purname').css({"border-color":""});
                $('#phone').focus();
                $('#phone').css({"border-color":"red"});
            } else if(purbillno == ''){
                $('#phone').css({"border-color":""});
                $('#purbillno').focus();
                $('#purbillno').css({"border-color":"red"});
            } else if(purbilldate == ''){
                $('#purbillno').css({"border-color":""});
                $('#purbilldate').focus();
                $('#purbilldate').css({"border-color":"red"});
            } else if(gst == ''){
                $('#purbilldate').css({"border-color":""});
                $('#gst').focus();
                $('#gst').css({"border-color":"red"});
            } else { 
                $('#gst').css({"border-color":""});
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                   type:'POST',
                   url:'temp/add',
                   data: {
                        billno: billno,
                        purid: purid,
                        item: item,
                        item_code: item_code,
                        item_rate: item_rate,
                        item_cgstp: item_cgstp,
                        item_sgstp: item_sgstp,
                        ime_1: ime_1,
                        ime_2: ime_2,
                        item_qty: item_qty,
                        item_dis: item_dis,
                        item_cgst: item_cgst,
                        item_sgst: item_sgst,
                        item_total: item_total,
                        purbillno: purbillno,
                        purbilldate: purbilldate,
                        purname: purname,
                        purphone: purphone,
                        gst: gst,
                        pudate: pudate
                    },
                   success:function(data) {
                      window.location = 'purchaseadd';
                   }
                });
                var aiNew = oTable.fnAddData(['', item, ime_1, ime_2, item_code, item_rate, item_qty, item_dis, item_cgstp, item_cgst, item_sgstp, item_sgst, item_total ,'<a class="delete" href="">Delete</a>' ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                // savedataRow(oTable, nRow);
                // $('#sales-table')[0].reset();
                nEditing = nRow;
                nNew = true;
            }            
        });

        table.on('click', '.deletepurchase', function (e) {
            e.preventDefault();

            if (confirm("Are you sure to delete this Item ?") == false) {
                return;
            }

            var nRow = $(this).parents('tr').attr('id');
            // alert(nRow);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
               type:'POST',
               url:'temp/delete/data',
               data: { id: nRow },
               success:function(data) {
                  $('#alert2').html(data.success);
                  window.location = 'purchaseadd';
               }
            });
            oTable.fnDeleteRow(nRow);
            // alert("Deleted! Do not forget to do some ajax to sync with backend :)");

        });

        table.on('click', '.cancelpurchase', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        // table.on('click', '.editsales', function (e) {
        //     e.preventDefault();
        //     nNew = false;
            
        //     /* Get the row as a parent of the link that was clicked on */
        //     var nRow = $(this).parents('tr')[0];

        //     if (nEditing !== null && nEditing != nRow) {
        //         /* Currently editing - but not this row - restore the old before continuing to edit mode */
        //         // $('#sample_editable_1_new_sales').addClass('display-hide')
        //         restoreRow(oTable, nEditing);
        //         editRow(oTable, nRow);
        //         nEditing = nRow;
        //         alert("backend :)");
        //     } else if (nEditing == nRow) {
        //         /* Editing this row and want to save it */
        //         saveRow(oTable, nEditing);
        //         nEditing = null;
        //         alert("Updated! Do not forget to do some ajax to sync with backend :)");
        //     } else {
        //         /* No edit in progress - let's start one */

        //         // $('#sample_editable_1_new_sales').addClass('display-hide')
        //         editRow(oTable, nRow);
        //         nEditing = nRow;
        //         alert("Updated:)");
        //     }
        // });
    }

    return {

        //main function to initiate the module
        init: function () {
            handleTable();
        }

    };

}();

jQuery(document).ready(function() {
    TableDatatablesEditable_purchase.init();
});