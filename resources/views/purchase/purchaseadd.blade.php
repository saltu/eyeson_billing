@extends('layouts.app')
@section('content')
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Purchase</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{ route('home') }}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Purchase</a>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
            @if(count($purchase_bill) > 0)
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>
                                    </div>
                                    <!-- <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="javascript:;" class="reload"> </a>
                                    </div> -->
                                </div>
                                <div class="portlet-body">
                                    
                                    <div class="row">                                    
                                        <div class="alert alert-danger display-hide" id="alert">
                                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                        </div>
                                        <div class="alert alert-success display-hide" id="alert2">
                                            <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                        </div>
                                    </div>
                                    <!-- BEGIN FORM-->
                                    <form action="{{ route('purchase.temp.details') }}" method="POST" class="form-horizontal">
                                        {{ csrf_field() }}
                                        <div class="form-body">
                                            <h3 class="form-section">Info</h3>
                                            <div class="row">                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Date</label>
                                                        <div class="col-md-9">
                                                            <input type="date" class="form-control" id="pudate" name="pudate" required value="{{ $purchase_bill[0]->bill_date }}" placeholder="Dadte">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                
                                                <!-- Bill number automatic -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Bill</label>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" id="billno" name="billno" readonly value="{{ $purchase_bill[0]->bill_no }}" placeholder="Automatic">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <!-- Name -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Name</label>
                                                        <?php 
                                                            $purchase_bill_temps_va = DB::table('purchasers')
                                                                        ->select('*')
                                                                        ->where('id',$purchase_bill[0]->purchaser_id)
                                                                        ->get();
                                                        ?>
                                                        <input type="hidden" value="{{ $purchase_bill_temps_va[0]->id }}" class="form-control purid" id="purid" name="purid">
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control purname" id="purname" name="purname" value="{{ $purchase_bill_temps_va[0]->name }}" placeholder="Customer name">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <!-- phone number  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Phone</label>
                                                        <div class="col-md-9">
                                                            <input type="number" value="{{ $purchase_bill_temps_va[0]->phone }}" class="form-control phone" id="phone" name="phone" placeholder="Phone number ">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->                                            
                                            <br>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Purchase Bill No</label>
                                                        <div class="col-md-9">
                                                            <input type="text" required value="{{ $purchase_bill[0]->purbillno }}" class="form-control" id="purbillno" name="purbillno">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Purchase Bill Date</label>
                                                        <div class="col-md-9">
                                                            <input type="date" required value="{{ $purchase_bill[0]->purbilldate }}" class="form-control" id="purbilldate" name="purbilldate">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Purchaser GST</label>
                                                        <div class="col-md-9">
                                                            <input type="text" required value="{{ $purchase_bill_temps_va[0]->gst }}" class="form-control" id="gst" name="gst">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->                                          
                                            <br>
                                            <br>
                                            <div class="row">                 
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Discount</label>
                                                        <div class="col-md-9">
                                                            <?php 
                                                                $purchase_bill_temps_va_di = DB::table('purchase_bill_temps')
                                                                            ->select('*')
                                                                            ->where('bill_no',$purchase_bill[0]->bill_no)
                                                                            ->orderBy('id', 'DESC')
                                                                            ->get();
                                                            ?>
                                                            <input type="text" value="{{ $purchase_bill_temps_va_di[0]->discount }}" class="form-control" min="0" id="pur_discount" name="pur_discount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <!--/row-->
                                            
                                            
                                            <?php
                                                $cgst = 0;
                                                $sgst = 0;
                                                $total = 0;
                                                $dis = 0;
                                                $dis_am = 0;
                                                $gran_tot = 0;
                                                foreach($purchase_bill as $purchase_bil){
                                                    $cgst += $purchase_bil->cgst;
                                                    $sgst += $purchase_bil->sgst;
                                                    $dis += $purchase_bil->discount;
                                                    $total += $purchase_bil->total;
                                                }

                                                    $dis_am = $purchase_bill_temps_va_di[0]->discount;
                                                    $gran_tott = $total;
                                                    $gran_tot = $total - $dis_am;
                                            ?>
                                            <div class="row">                 
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">CGST</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly value="{{ $cgst }}" class="form-control" id="pursubcgst" name="pursubcgst">
                                                            <input type="hidden" readonly value="{{ $cgst }}" class="form-control" id="pursubcgsttem" name="pursubcgsttem">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">SGST</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly value="{{ $sgst }}" class="form-control" id="pursubsgst" name="pursubsgst">
                                                            <input type="hidden" readonly value="{{ $sgst }}" class="form-control" id="pursubsgsttem" name="pursubsgsttem">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">SubTotal</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly value="{{ $gran_tot }}" class="form-control" id="pursubtotal" name="pursubtotal">
                                                            <input type="hidden" readonly value="{{ $gran_tott }}" class="form-control" id="pursubtotaltem" name="pursubtotaltem">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            
                                            <h3 class="form-section">Items</h3>
                                            <!--/row-->
                                            <div class="row" >
                                                <div class="col-md-12">
                                                    <div class="portlet-body">
                                                        <div class="table-responsive" style="height:170px !important;" >
                                                            <table class="table table-striped table-bordered table-hover">
                                                                <thead>
                                                                    <tr class="d-flex" >
                                                                        <th class ="text-center col-md-2"> Item </th>
                                                                        <th class ="text-center "> Code </th>
                                                                        <th class="text-center col-md-1"> Rate </th>
                                                                        <th class="text-center col-md-1"> Qty </th>
                                                                        <th class="text-center col-md-1"> CGST % </th>
                                                                        <th class="text-center col-md-1"> CGST </th>
                                                                        <th class="text-center col-md-1"> SGST % </th>
                                                                        <th class="text-center col-md-1"> SGST </th>
                                                                        <th class="text-center col-md-1"> IGST </th>
                                                                        <th class="text-center"> Total </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr id="purchase-table">
                                                                        <td><input type="text" id="item_name" name="item_name" placeholder="Product Name" class="form-control form-filter input-sm item_name" />
                                                                        <input type="text" placeholder="Product IME 1" id="ime_1" name="ime_1" class="form-control form-filter input-sm ime_1" />
                                                                        <input placeholder="Product IME 2" type="text" id="ime_2" name="ime_2" class="form-control form-filter input-sm ime_2" />
                                                                        </td>
                                                                        <td> <input type="text" class="form-control form-filter input-sm" id="itempu-code" name="itempu-code" maxlength="12"> </td>
                                                                        <td> <input type="number" min="0" class="form-control form-filter input-sm" id="itempu-rate" name="itempu-rate"> </td>
                                                                        <td> <input type="number" min="0" class="form-control form-filter input-sm" id="itempu-qty" name="itempu-qty">
                                                                        </td>
                                                                        <td> <input type="number" class="form-control form-filter input-sm" id="itempu-cgstp" name="itempu-cgstp"> </td>
                                                                        <td> <input readonly type="text" class="form-control form-filter input-sm" id="itempu-cgst" name="itempu-cgst"> </td>
                                                                        <td> <input type="text" readonly class="form-control form-filter input-sm" id="itempu-sgstp" name="itempu-sgstp"> </td>
                                                                        <td> <input readonly type="text" class="form-control form-filter input-sm" id="itempu-sgst" name="itempu-sgst"> </td>
                                                                        <td> <input readonly type="text" class="form-control form-filter input-sm" id="itempu-igst" name="itempu-igst"> </td>
                                                                        <td> <input type="text" readonly class="form-control form-filter input-sm" id="itempu-totall" name="itempu-totall"> </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        
                                                        <center>
                                                            <div class="row">                                   
                                                                <div class="alert alert-danger display-hide" id="alert25">
                                                                    <button class="close" data-close="alert"></button> You have some form errors. Please check above details. 
                                                                </div>
                                                            </div>
                                                        <div class="btn-group">
                                                            <button type="submit" id="sample_editable_1_new_purchase" class="btn green"> Add
                                                            <i class="fa fa-plus"></i>
                                                            </button>
                                                        </div>
                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <div class="portlet-body">
                                                <table class="table table-striped table-hover table-bordered" id="sample_editable_1_purchase">
                                                    <thead>
                                                        <tr class="d-flex" >
                                                            <th> # </th>
                                                            <th class ="text-center"> Item </th>
                                                            <th class ="text-center"> IME 1 </th>
                                                            <th class ="text-center"> IME 2 </th>
                                                            <th class ="text-center"> Code </th>
                                                            <th class ="text-center"> Rate </th>
                                                            <th class ="text-center"> Qty </th>
                                                            <th class ="text-center"> CGST % </th>
                                                            <th class ="text-center"> CGST </th>
                                                            <th class ="text-center"> SGST % </th>
                                                            <th class ="text-center"> SGST </th>
                                                            <th class ="text-center"> IGST </th>
                                                            <th class ="text-center"> Total </th>
                                                            <th class ="text-center"> Delete </th>            
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php $i = 1; ?>
                                                        @if(count($purchase_bill_temps) > 0)
                                                            @foreach($purchase_bill_temps as $purchase_bill_temp)
                                                                <tr id="{{ $purchase_bill_temp->id }}">
                                                                    <td> {{ $i++ }} </td>
                                                                    <td> {{ $purchase_bill_temp->product_name }} </td>
                                                                    <td> {{ $purchase_bill_temp->ime_1 }} </td>
                                                                    <td> {{ $purchase_bill_temp->ime_2 }} </td>
                                                                    <td> {{ $purchase_bill_temp->product_code }} </td>
                                                                    <td> {{ $purchase_bill_temp->product_rate }} </td>
                                                                    <td> {{ $purchase_bill_temp->quantity }} </td>
                                                                    <td> {{ $purchase_bill_temp->cgstp }} </td>
                                                                    <td> {{ $purchase_bill_temp->cgst }} </td>
                                                                    <td> {{ $purchase_bill_temp->sgstp }} </td>
                                                                    <td> {{ $purchase_bill_temp->sgst }} </td>
                                                                    <td>--</td>
                                                                    <td> {{ $purchase_bill_temp->total }} </td>
                                                                    <td>
                                                                        <a class="deletepurchase" href="javascript:;"> Delete </a>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <center>
                                        <div class="form-actions">
                                            <button type="submit" class="btn green submitq">Submit</button>
                                            <a href="{{ route('purchase.temp.delete') }}" onclick="javascript:check=confirm( 'Do You Want To Cancel the Bill?'); if(check==false) return false;"><button type="button" class="btn default">Delete</button></a>
                                        </div>
                                        </center>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            @else
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>
                                    </div>
                                    <!-- <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="javascript:;" class="reload"> </a>
                                    </div> -->
                                </div>
                                <div class="portlet-body">
                                    
                                    <div class="row">                                    
                                        <div class="alert alert-danger display-hide" id="alert">
                                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                        </div>
                                        <div class="alert alert-success display-hide" id="alert2">
                                            <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                        </div>
                                    </div>
                                    <!-- BEGIN FORM-->
                                    <!-- <form action="#" class="form-horizontal"> -->
                                        <div class="form-body">
                                            <h3 class="form-section">Info</h3>
                                            <div class="row">                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Date</label>
                                                        <div class="col-md-9">
                                                            <input type="date" class="form-control" id="pudate" name="pudate" required placeholder="Dadte">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">                                                
                                                <!-- Bill number automatic -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Bill</label>
                                                        <div class="col-md-9">
                                                            <input type="text" required class="form-control" id="billno" name="billno" readonly value="{{ $innum }}" placeholder="Automatic">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <!-- Name -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Name</label>
                                                        <div class="col-md-9">
                                                            <input type="hidden" class="form-control purid" id="purid" name="purid">
                                                            <input type="text" required class="form-control purname" id="purname" name="purname" placeholder="Purchaser name">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <!-- phone number  -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Phone</label>
                                                        <div class="col-md-9">
                                                            <input type="number" required class="form-control phone" id="phone" name="phone" placeholder="Phone number ">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->                                            
                                            <br>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Purchase Bill No</label>
                                                        <div class="col-md-9">
                                                            <input type="text" required class="form-control" id="purbillno" name="purbillno">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Purchase Bill Date</label>
                                                        <div class="col-md-9">
                                                            <input type="date" required class="form-control" id="purbilldate" name="purbilldate">
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Purchaser GST</label>
                                                        <div class="col-md-9">
                                                            <input type="text" required class="form-control" id="gst" name="gst">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row--> 
                                            <br>
                                            <br>
                                            <div class="row">                 
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Discount</label>
                                                        <div class="col-md-9">
                                                            <input type="text" value="0" class="form-control" min="0" id="pur_discount" name="pur_discount">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <!--/row--> 
                                            <div class="row">                 
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">CGST</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly value="0" class="form-control" id="pursubcgst" name="pursubcgst">
                                                            <input type="hidden" readonly value="0" class="form-control" id="pursubcgsttem" name="pursubcgsttem">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">SGST</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly value="0" class="form-control" id="pursubsgst" name="pursubsgst">
                                                            <input type="hidden" readonly value="0" class="form-control" id="pursubsgsttem" name="pursubsgsttem">
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">SubTotal</label>
                                                        <div class="col-md-9">
                                                            <input type="text" readonly value="0" class="form-control" id="pursubtotal" name="pursubtotal">
                                                            <input type="hidden" readonly value="0" class="form-control" id="pursubtotaltem" name="pursubtotaltem">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            
                                            <h3 class="form-section">Items</h3>
                                            <!--/row-->
                                            <div class="row" >
                                                <div class="col-md-12">
                                                    <div class="portlet-body">
                                                        <div class="table-responsive" style="height:170px !important;" >
                                                            <table class="table table-striped table-bordered table-hover">
                                                                <thead>
                                                                    <tr class="d-flex" >
                                                                        <th class ="text-center col-md-2"> Item </th>
                                                                        <th class ="text-center col-md-1.5"> Code </th>
                                                                        <th class="text-center col-md-1"> Rate </th>
                                                                        <th class="text-center col-md-1"> Qty </th>
                                                                        <th class="text-center col-md-1"> CGST % </th>
                                                                        <th class="text-center col-md-1"> CGST </th>
                                                                        <th class="text-center col-md-1"> SGST % </th>
                                                                        <th class="text-center col-md-1"> SGST </th>
                                                                        <th class="text-center col-md-1"> IGST </th>
                                                                        <th class="text-center"> Total </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr id="purchase-table">
                                                                        <td><input required type="text" id="item_name" name="item_name" placeholder="Product Name" class="form-control form-filter input-sm item_name" />
                                                                        <input required type="text" placeholder="Product IME 1" id="ime_1" name="ime_1" class="form-control form-filter input-sm ime_1" />
                                                                        <input placeholder="Product IME 2" type="text" id="ime_2" name="ime_2" class="form-control form-filter input-sm ime_2" />
                                                                        </td>
                                                                        <td> <input required type="text" class="form-control form-filter input-sm" id="itempu-code" name="itempu-code" maxlength="12"> </td>
                                                                        <td> <input required type="number" min="0" class="form-control form-filter input-sm" id="itempu-rate" name="itempu-rate"> </td>
                                                                        <td> <input required type="number" min="0" class="form-control form-filter input-sm" id="itempu-qty" name="itempu-qty">
                                                                        </td>
                                                                        <td> <input required type="number" class="form-control form-filter input-sm" id="itempu-cgstp" name="itempu-cgstp"> </td>
                                                                        <td> <input required readonly type="text" class="form-control form-filter input-sm" id="itempu-cgst" name="itempu-cgst"> </td>
                                                                        <td> <input required type="text" readonly class="form-control form-filter input-sm" id="itempu-sgstp" name="itempu-sgstp"> </td>
                                                                        <td> <input required readonly type="text" class="form-control form-filter input-sm" id="itempu-sgst" name="itempu-sgst"> </td>                                 <td> <input required readonly type="text" class="form-control form-filter input-sm" id="itempu-igst" name="itempu-igst"> </td>
                                                                        <td> <input required type="text" readonly class="form-control form-filter input-sm" id="itempu-totall" name="itempu-totall"> </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        
                                                        <center>
                                                        <div class="btn-group">
                                                            <button id="sample_editable_1_new_purchase" class="btn green"> Add
                                                            <i class="fa fa-plus"></i>
                                                            </button>
                                                        </div>
                                                        </center>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <div class="portlet-body">
                                                <table class="table table-striped table-hover table-bordered " id="sample_editable_1_purchase">
                                                    <thead>
                                                        <tr class="d-flex" >
                                                            <th> # </th>
                                                            <th class ="text-center"> Item </th>
                                                            <th class ="text-center"> IME 1 </th>
                                                            <th class ="text-center"> IME 2 </th>
                                                            <th class ="text-center"> Code </th>
                                                            <th class ="text-center"> Rate </th>
                                                            <th class ="text-center"> Qty </th>
                                                            <th class ="text-center"> CGST % </th>
                                                            <th class ="text-center"> CGST </th>
                                                            <th class ="text-center"> SGST % </th>
                                                            <th class ="text-center"> SGST </th>
                                                            <th class ="text-center"> IGST </th>
                                                            <th class ="text-center"> Total </th>
                                                            <!-- <th class="text-center"> Delete</th> -->
                                                            
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- <center>
                                        <div class="form-actions">
                                            <button type="submit" class="btn green">Submit</button>
                                            <button type="button" class="btn default">Cancel</button>
                                        </div>
                                        </center> -->
                                    <!-- </form> -->
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
@endsection