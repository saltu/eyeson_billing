@extends('layouts.app')
@section('content')
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="index.html">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Purchase</a>
            </li>
        </ul>
    </div>
    <div style="width: 280mm; height:auto; margin: 0 auto;" >
        <table width="100%" style="border:1px solid;" cellspacing="0" cellpadding="0">
            <tr>
                <td style="height:30px; border-bottom:1px solid;" valign="top">
                    <h1 style="text-align:center; font-weight:bolder; text-transform:uppercase; margin-top:3px; margin-bottom:3px;"><b>Tax Invoice</b></h1>
                </td>
            </tr>
            <tr>
                <td style="height:60px;" valign="top">                                    
                    <table width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="50%" style="height:60px; border-right:1px solid;">
                                <span style="position:absolute; margin-top: 0px;"><img src="{{ asset('resources/assets/images/logo_black.png') }}" style="width: 220px; margin-top:0px; margin-left:160px;" alt="logo"></span>
<!--                                <p style="font-size:25px; font-weight:bolder; text-align:center;">EyesOn Mobiles</p>-->
                                <p style="text-align:center; margin-top:60px !important; font-size:12px;">GSTIN Number: 32BPQPV2449G1ZI</p>
                            </td>
                            <td width="50%" style="height:60px;" valign="top">
                                <p style="text-align:center; font-weight:bold; margin-bottom:0px; margin-top:4px;">Nangiarkulangara, Haripad, Alappuzha District </p>
                                <p style="text-align:center; font-weight:bold; margin-top:4px; margin-bottom:0px;"> Kerala - 690516</p>
                                <p style="text-align:center; font-weight:bold; margin-bottom:0px; margin-top:4px;"><i class="fa fa-phone" aria-hidden="true"></i>940 084 2092</p>
                                <p style="text-align:center; font-weight:bold;  margin-top:4px; margin-bottom:4px;"><i class="fa fa-envelope-o" aria-hidden="true"></i> | <i class="fa fa-globe" aria-hidden="true"></i></p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <table width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="50%" style="border-top:1px solid; height:25px; border-right:1px solid; background-color:#eaeaea; text-align:center;">Details of Dealer (Billed to)</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <table width="100%" cellspacing="0" cellpadding="0">
                       <?php 
                            $purchase_bill_temps_va = DB::table('purchasers')
                                        ->select('*')
                                        ->where('id',$purchase_bill[0]->purchaser_id)
                                        ->get();
                        ?> 
                        <tr>
                            <td width="50%" style="border-top:1px solid; height:80px; border-right:1px solid; padding-left:40px;" valign="top">
                                <!--Billed To Details -->
                                <table >
                                    <tr>
                                        <td style="height:20px;">Invoice Number </td>
                                        <td>: {{ $purchase_bill[0]->bill_no }}</td>
                                    </tr>
                                    <tr>
                                        <td style="height:20px;">Consignor</td>
                                        <td>: {{ $purchase_bill_temps_va[0]->name}}</td>
                                    </tr>
                                    <tr>
                                        <td style="height:20px;">GSTIN Number</td>
                                        <td>: {{ $purchase_bill_temps_va[0]->gst }}</td>
                                    </tr>
                                </table>
                                <!--Billed To Details -->
                            </td>
                            <td width="50%" style="border-top:1px solid; padding-left:40px;" valign="top">                                                
                                                        <!--Shipped To Details -->                       
                                <table >
                                    <tr>
                                        <td style="height:20px;">Buyer's Order no.</td>
                                        <td>: {{ $purchase_bill[0]->purbillno }}</td>
                                    </tr>
                                    <tr>
                                        <td style="height:20px;">Phone</td>
                                        <td>: {{ $purchase_bill_temps_va[0]->phone }}</td>
                                    </tr>
                                    <tr>
                                        <td style="height:20px;">Invoice Date</td>
                                        <td>: {{ $purchase_bill[0]->bill_date }}</td>
                                    </tr>
                                </table>
                                <!--Shipped To Details -->                          
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td style="border-top:1px solid; border-right:1px solid; height:20px; text-align:center;" width="3%">No</td>
                            <td width="24%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:20px;"> Item </td>
                            <td width="10%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:20px;"> Code </td>
                            <td width="6%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:20px;">Qty</td>
                            <td width="10%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:20px;">RATE</td>
                            <td width="10%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:20px;">TOTAL <br> (A)</td>
                            <td width="10%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:35px;">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td colspan="2" style="border-bottom:1px solid; height:15px;" width="100%">CGST  (B)</td>
                                    </tr>
                                    <tr>
                                        <td width="30%" style="border-right:1px solid; height:20px;">%</td>
                                        <td width="70%">Amount</td>
                                    </tr>
                                </table>

                            </td>
                            <td width="10%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:35px;">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td colspan="2" style="border-bottom:1px solid; height:15px;" width="100%">SGST (C)</td>
                                    </tr>
                                    <tr>
                                        <td width="30%" style="border-right:1px solid; height:20px;">%</td>
                                        <td width="70%">Amount</td>
                                    </tr>
                                </table>

                            </td>
                            <td width="10%" style="border-top:1px solid;  text-align:center; height:35px;">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td colspan="2" style="border-bottom:1px solid; height:15px;" width="100%">IGST  (D)</td>
                                    </tr>
                                    <tr>
                                        <td width="30%" style="border-right:1px solid; height:20px;">%</td>
                                        <td width="70%">Amount</td>
                                    </tr>
                                </table>

                            </td>
                            <td width="8%" style="border-top:1px solid; border-left:1px solid; text-align:center; height:20px;">(A+B+C+D)  TOTAL</td>
                        </tr>
                    </table>
                </td>
            </tr>

            <?php $i = 1;
            $trate = 0;
            $tcess = 0;
            ?>
            @if(count($purchase_bill_temps) > 0)
                @foreach($purchase_bill_temps as $purchase_bill_temp)
                    <?php 
                        $trate +=  $purchase_bill_temp->quantity * $purchase_bill_temp->product_rate ;
//                        $tcess += ((($purchase_bill_temp->quantity * $purchase_bill_temp->product_rate)*1)/100) 
            
                    ?>
                    <tr>
                        <td width="100%">
                            <table cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td style="border-top:1px solid; border-right:1px solid; height:30px; text-align:center;" width="3%">{{ $i++ }}</td>
                                    <td width="24%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:30px;">{{ $purchase_bill_temp->product_name.'-'.$purchase_bill_temp->ime_1 }}</td>
                                    <td width="10%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:30px;">{{ $purchase_bill_temp->product_code }}</td>
                                    <td width="6%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:30px;">{{ $purchase_bill_temp->quantity }}</td>
                                    <td width="10%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:30px;">{{ $purchase_bill_temp->product_rate }}</td>
                                    <td width="10%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:30px;">{{ $purchase_bill_temp->quantity * $purchase_bill_temp->product_rate }}</td>
                                    <td width="10%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:30px;">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="30%" style="border-right:1px solid; height:35px;">{{ $purchase_bill_temp->cgstp }}</td>
                                                <td width="70%">{{ $purchase_bill_temp->cgst }}</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="10%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:30px;">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="30%" style="border-right:1px solid; height:35px;">{{ $purchase_bill_temp->sgstp }}</td>
                                                <td width="70%">{{ $purchase_bill_temp->sgst }}</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="10%" style="border-top:1px solid;  text-align:center; height:35px;">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td width="30%" style="border-right:1px solid; height:35px;">--</td>
                                                <td width="70%">--</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="8%" style="border-top:1px solid; border-left:1px solid;  text-align:center; height:30px;">{{ $purchase_bill_temp->total }}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                @endforeach
             @endif   

            <?php
            
                $rate = $purchase_bill_temp->product_rate;
                $cgst = $purchase_bill[0]->tcgst;
                $sgst = $purchase_bill[0]->tsgst;
                $total = $purchase_bill[0]->total;
            
                
                
               # $product_tot = $total - ($cgst+$sgst);
                $product_tot  = $trate;

                $dis = $purchase_bill[0]->discount;
                $dis_am = 0;
                $gran_tot = 0;

                    $dis_am = $dis;
                    $rnd_total_am = round($total);
                    $rnd_am = round(($rnd_total_am - $total),2);

                    $gran_tot  = $rnd_total_am - $dis_am;
                    $decimal=0 ;
                    $decimal = round($gran_tot - ($no = floor($gran_tot)), 2) * 100;
                    $hundred = null;
                    $digits_length = strlen($no);
                    $i = 0;
                    $str = array();
                    $words = array(0 => '', 1 => 'One', 2 => 'Two',
                        3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
                        7 => 'seven', 8 => 'Eight', 9 => 'Nine',
                        10 => 'Ten', 11 => 'Eleven', 12 => 'Twelve',
                        13 => 'Thirteen', 14 => 'Fourteen', 15 => 'Fifteen',
                        16 => 'Sixteen', 17 => 'Seventeen', 18 => 'Eighteen',
                        19 => 'Nineteen', 20 => 'Twenty', 30 => 'Thirty',
                        40 => 'Forty', 50 => 'Fifty', 60 => 'Sixty',
                        70 => 'Seventy', 80 => 'Eighty', 90 => 'Ninety');
                    $digits = array('', 'Hundred','Thousand','Lakh', 'Crore');
                    while( $i < $digits_length ) {
                        $divider = ($i == 2) ? 10 : 100;
                        $number = floor($no % $divider);
                        $no = floor($no / $divider);
                        $i += $divider == 10 ? 1 : 2;
                        if ($number) {
                            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                            $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
                        } else $str[] = null;
                    }
                    $Rupees = implode('', array_reverse($str));
                    $paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
//                        return ($Rupees ? $Rupees . 'Rupees ' : '') . $paise .; 
                    $amount =  ($Rupees ? $Rupees . 'Rupees ' : '') . $paise ."" . "Only";
            ?>
            <tr>
                <td width="100%">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td style="border-top:1px solid; border-right:1px solid; height:30px; text-align:center;" width="3%"></td>
                            <td width="24%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:30px;"></td>
                            <td width="10%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:30px;"></td>
                            <td width="6%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:30px;"></td>
                            <td width="10%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:30px;"></td>
                            <td width="10%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:30px;"></td>
                            <td width="10%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:30px;">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="30%" style="border-right:1px solid; height:30px;"></td>
                                        <td width="70%"></td>
                                    </tr>
                                </table>
                            </td>
                            <td width="10%" style="border-top:1px solid; border-right:1px solid; text-align:center; height:30px;">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="30%" style="border-right:1px solid; height:30px;"></td>
                                        <td width="70%"></td>
                                    </tr>
                                </table>
                            </td>
                            <td width="10%" style="border-top:1px solid;  text-align:center; height:30px;">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="30%" style="border-right:1px solid; height:30px;"></td>
                                        <td width="70%"></td>
                                    </tr>
                                </table>
                            </td>
                            <td width="8%" style="border-top:1px solid; border-left:1px solid;  text-align:center; height:30px;"></td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td width="100%">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td style="border-top:1px solid;  height:35px; text-align:center;" width="43%">
                             </td>
                            <td style="border-top:1px solid;  border-left:1px solid; text-align:center; height:20px; font-weight: bold;" width="10.1%">
                            TOTAL</td>
                            <td style="border-top:1px solid;  border-left:1px solid; text-align:center; height:20px; font-weight: bold;" width="10%">
                            {{ $product_tot }}</td>
                            <td style="border-top:1px solid;  border-left:1px solid; text-align:center; height:20px; font-weight: bold;" width="10%">
                            {{$cgst}}</td>
                            <td style="border-top:1px solid;  border-left:1px solid; text-align:center; height:20px; font-weight: bold;" width="10%">
                            {{$sgst}}</td>
                            <td style="border-top:1px solid;  border-left:1px solid; text-align:center; height:20px; font-weight: bold;" width="10%">
                            --</td>
                            <td style="border-top:1px solid; border-left:1px solid;  text-align:center; height:20px; font-weight: bold;">{{$total}}</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td style="border-top:1px solid;  height:35px; text-align:center;" width="83%">
                             </td>
                            <td style="border-top:1px solid;  border-left:1px solid; text-align:center; height:20px; font-weight: bold;" width="10%">
                            Disount</td>
                            <td style="border-top:1px solid;  border-left:1px solid; text-align:center; height:20px; font-weight: bold;" width="10%">
                            -({{$dis}})</td>

                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td style="border-top:1px solid;  height:35px; text-align:center;" width="83%">
                             </td>
                            <td style="border-top:1px solid;  border-left:1px solid; text-align:center; height:20px; font-weight: bold;" width="10%">
                            Round value</td>
                            <td style="border-top:1px solid;  border-left:1px solid; text-align:center; height:20px; font-weight: bold;" width="10%">
                            ({{$rnd_am}})</td>

                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td style="border-top:1px solid;  height:35px; text-align:center;" width="83%">
                             </td>
                            <td style="border-top:1px solid;  border-left:1px solid; text-align:center; height:20px; font-weight: bold;" width="10%">
                            Grand Total</td>
                            <td style="border-top:1px solid;  border-left:1px solid; text-align:center; height:20px; font-weight: bold;" width="10%">
                            {{$gran_tot}}</td>

                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td style="border-top:1px solid;  height:35px; text-align:right;" width="43.2%">
                              <div>
                                <p style="float: left; padding-left:30px;"> Total Amount in words : </p>
                                <p style="float: right; padding-right:20px; font-weight: bold;">  {{$amount}} </p>
                              </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <table cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td style="border-top:1px solid; border-right:1px solid;  height:35px; padding-left:10px;" width="50%">
                                <p style="text-align:left:">Note:</p>
                                <p style="text-align:left:">1. Warranty for the item will be provided from the respective companies.</p>
                                <p style="text-align:left:">2. Thank you for your Business</p>
                            </td>

                            <td width="50%" style="border-top:1px solid;  text-align:center; height:20px;">
                                 <p style="text-align:left:">Authorised Signatory:</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>   
    </div>
    <br>
    <div class="container">
         <div class="row" id="non-printable" style="position: relative; left: 40%;" >
            <div class="col-md-12">
                <a class="btn btn-lg green-haze hidden-print uppercase print-btn" onclick="window.print();">Print <i class="fa fa-print"></i></a>
                <a class="btn btn-lg green-haze hidden-print uppercase print-btn" href="{{ route('purchase.purchasereport') }}">Go Back</a>                            
            </div>
        </div>
    </div> 
</div>
@endsection