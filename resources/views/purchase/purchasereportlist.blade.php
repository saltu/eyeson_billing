<script src="{{ asset('resources/assets/pages/scripts/table-datatables-editable-purchasereport.js') }}" type="text/javascript"></script>
<table class="table table-striped table-hover table-bordered" id="sample_editable_1_purchasereport">
    <thead>
        <tr class="d-flex">
            <th> # </th>
            <th class="text-center"> Bill No </th>
            <th class="text-center"> Purchaser Name </th>
            <th class ="text-center col-md-2"> Grand Total </th>
            <th class ="text-center"> Amount Paid </th>
            <th class="text-center"> Bill Date </th>
            <th class="text-center"> Action </th>
            <!-- <th class="text-center"> Delete</th> -->
        </tr>
    </thead>
    <tbody>
       <?php $i = 1; ?>
        @if(count($purchase_bill_data) > 0)
            @foreach($purchase_bill_data as $purchase_bill)
                <tr>
                    <td> {{ $i++ }} </td>
                    <td> {{ $purchase_bill->bill_no }} </td>
                    <?php 
                        $purchaser = DB::table('purchasers')
                                    ->select('*')
                                    ->where('id',$purchase_bill->purchaser_id)
                                    ->get();
                    ?>
                    <td> {{ $purchaser[0]->name }} </td>
                    <td> {{ round($purchase_bill->total) }} </td>
                    <td> {{ $purchase_bill->amountpay + $purchase_bill->amountpayl }} </td>
                    <td> {{ $purchase_bill->bill_date }} </td>
                    <td> <a href="{{ route('purchase.purchasereport.details', $purchase_bill->bill_no) }}">View</a> </td>
                    <!-- <td> <a href="">Delete</a> </td> -->
                </tr>
            @endforeach
        @endif 
    </tbody>
</table>