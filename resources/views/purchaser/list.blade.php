@extends('layouts.app')
@section('content')
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Purchaser
                <small>  </small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE TOOLBAR -->
    </div>
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{ route('home') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Purchaser</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-settings font-red"></i>
                                <span class="caption-subject font-red sbold uppercase">Purchaser Details</span>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"> </a>
                                <a href="javascript:;" class="reload"> </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">                                    
                                    <div class="alert alert-danger display-hide" id="alert">
                                        <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                    </div>
                                    <div class="alert alert-success display-hide" id="alert2">
                                        <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                    </div>
                                </div>
                                <div class="row">                                    
                                    <div class="col-md-6">
                                        <div class="btn-group">
                                            <button id="sample_editable_1_new_pur" class="btn green"> Add New
                                            <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-6">
                                        <div class="btn-group pull-right">
                                            <button class="btn green btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                            <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;"> Print </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Save as PDF </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> Export to Excel </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <table class="table table-striped table-hover table-bordered" id="sample_editable_1_pur">
                                <thead>
                                    <tr>
                                        <th> Sl.No </th>
                                        <th> Name </th>
                                        <th> Phone </th>
                                        <th> GST TIN </th>
                                        <th> Edit </th>
                                        <th> Delete </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    @if(count($purchasers) > 0 )
                                        @foreach ($purchasers as $purchaser)
                                            <tr id="{{ $purchaser->id }}">
                                                <td class="text-center"> {{ $i++ }} </td>
                                                <td class="text-center"> {{ $purchaser->name }} </td>
                                                <td class="text-center"> {{ $purchaser->phone }} </td>
                                                <td class="text-center"> {{ $purchaser->gst }} </td>
                                                <td class="text-center"><a class="editpur" href="javascript:;"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                                                <td class="text-center"><a class="deletepur" href="javascript:;"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>                
            </div>
        </div>
    </div>
<!-- END PAGE CONTENT INNER -->
</div>
<!-- END PAGE CONTENT BODY -->
@endsection