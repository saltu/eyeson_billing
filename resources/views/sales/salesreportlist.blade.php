<script src="{{ asset('resources/assets/pages/scripts/table-datatables-editable-salesreport.js') }}" type="text/javascript"></script>
<table class="table table-striped table-hover table-bordered" id="sample_editable_1_salesreport">
    <thead>
        <tr class="d-flex" >
            <th> # </th>
            <th class="text-center"> Bill No </th>
            <th class="text-center"> Customer Name </th>
            <th class ="text-center col-md-2"> Grand Total </th>
            <th class ="text-center"> Amount Paid </th>
            <th class="text-center"> Bill Date </th>
            <th class="text-center"> Action </th>
            <!-- <th class="text-center"> Delete</th> -->
        </tr>
    </thead>
    <tbody>
       <?php $i = 1; ?>
        @if(count($sales_bill_data) > 0)
            @foreach($sales_bill_data as $sales_bill)
                <tr>
                    <td> {{ $i++ }} </td>
                    <td> {{ $sales_bill->bill_no }} </td>
                    <?php 
                        $customers = DB::table('customers')
                                    ->select('*')
                                    ->where('id',$sales_bill->customer_id)
                                    ->get();
                    ?>
                    <td> {{ $customers[0]->name }} </td>
                    <td> {{ round($sales_bill->total) }} </td>
                    <td> {{ $sales_bill->amountpay + $sales_bill->amountpayl }} </td>
                    <td> {{ $sales_bill->bill_date }} </td>
                    <td> <a href="{{ route('sales.salesreport.details', $sales_bill->bill_no) }}">View</a> </td>
                    <!-- <td> <a href="">Delete</a> </td> -->
                </tr>
            @endforeach
        @endif 
    </tbody>
</table>