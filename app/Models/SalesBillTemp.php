<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesBillTemp extends Model
{
    protected $fillable = [
        'bill_date', 'bill_no', 'customer_id', 'product_id','rate','cgst','sgst','cess','quantity','total','amountpay','amountpayl'
    ];

    protected $hidden = [];
    
}
