<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseBillTemp extends Model
{
    protected $fillable = [
        'bill_date', 'bill_no', 'purbillno',  'purbilldate',  'purchaser_id', 'product_code','product_name', 'cgstp', 'sgstp', 'product_rate', 'ime_1', 'ime_2', 'cgst','sgst','quantity','discount','total','amountpay','amountpayl'
    ];

    protected $hidden = [];
}
