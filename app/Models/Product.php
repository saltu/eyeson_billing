<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'code', 'name', 'purchased','gst','quantity','rate','ime_1','ime_2'
    ];

    protected $hidden = [];
}
