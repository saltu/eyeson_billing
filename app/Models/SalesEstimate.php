<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesEstimate extends Model
{
    protected $fillable = [
        'bill_date', 'customer_name' , 'customer_phone' , 'product_name', 'product_id','rate','quantity','total'
    ];

    protected $hidden = [];
}
