<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Purchaser extends Model
{
    protected $fillable = [
        'name', 'phone', 'amount', 'gst'
    ];

    protected $hidden = [];
}
