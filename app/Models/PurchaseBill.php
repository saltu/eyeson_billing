<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseBill extends Model
{
   protected $fillable = [
        'bill_id', 'bill_no', 'product_code','product_name', 'cgstp', 'sgstp', 'product_rate', 'ime_1', 'ime_2', 'cgst','sgst','quantity','total'
    ];

    protected $hidden = [];
}
