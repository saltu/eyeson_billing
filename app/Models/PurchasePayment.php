<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchasePayment extends Model
{
    protected $fillable = [
        'bill_no','desc', 'billdate', 'mode', 'ref', 'amount'
    ];

    protected $hidden = [];
    
}
