<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesBill extends Model
{
    protected $fillable = [
        'bill_id', 'bill_no', 'product_id','rate','quantity'
    ];

    protected $hidden = [];
}
