<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesBillDetail extends Model
{
    protected $fillable = [
        'bill_date', 'bill_no', 'customer_id', 'tcgst', 'tsgst','tcess','total','amountpay','amountpayl'
    ];

    protected $hidden = [];
}
