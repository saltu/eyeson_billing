<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use Auth;
use DB;
use Session;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class DailyReportController extends Controller
{
	public function index(){
		return view('dailyreport.report');
	}

	public function reportdetails(Request $request){
		// echo $request->dailydate;die;
		$dailydate = $request->dailydate;


		$opening = DB::table('open_closes')
						->select('*')
						->where('current_date',$dailydate)
						->get();

		$sales_bills = DB::table('sales_payments')
							->select('*')
							->where('billdate',$dailydate)
							->get();
		$purchase_bills = DB::table('purchase_payments')
							->select('*')
							->where('billdate',$dailydate)
							->get();
		$expenses = DB::table('expenses')
							->select('*')
							->where('ex_date',$dailydate)
							->orderBy('id','DESC')
							->get();
		// echo print_r($sales_bills);die;
		return view('dailyreport.reportlist', compact('opening', 'sales_bills','purchase_bills','expenses'));
	}
}
