<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\User;
use Illuminate\Http\Request;
use Hash;
use Auth;
use Redirect;
use Session;
use Validator;
use Illuminate\Support\Facades\Input;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index(){
        // $renew = DB::table('')->select('*')->get();
        return view('login');
    }
    
    public function login(Request $request){
        $rules = array (
                
                'username' => 'required',
                'password' => 'required' 
        );
        $validator = Validator::make ( Input::all (), $rules );
        if ($validator->fails ()) {
            return Redirect::back ()->withErrors ($validator)->withInput ();
        } else {
            if (Auth::attempt ( array (
                    
                    'username' => $request->get ( 'username' ),
                    'password' => $request->get ( 'password' ) 
            ) )) {
                session ( [ 
                        
                        'username' => $request->get ( 'username' ) 
                ] );
                return Redirect('/home');
            } else {
                return redirect()->back()->with('message', "Invalid Credentials , Please try again.");
            }
        }
    }

    public function logout() {
        Session::flush ();
        Auth::logout ();
        return Redirect('/');
    }
}
