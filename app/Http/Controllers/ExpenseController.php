<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Expense;
use Auth;
use DB;
use Session;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class ExpenseController extends Controller
{
    public function index(){
    	$expenses = DB::table('expenses')
						->select('*')
						->get();
    	return view('expense.list', compact('expenses'));
    }

    public function add(Request $request){

    	// echo $request->code.$request->name.$request->purchased.$request->gst.$request->quantity.$request->rate;die;

		if (Expense::where('id', $request->id)->count() > 0) {
			DB::update("UPDATE expenses SET description = '$request->description', amount = '$request->amount' WHERE id = ? ",[$request->id]);
    		return response()->json(['success'=>'Data is successfully Edited']);  
		} else {
			$expense = new Expense();
			$expense->description = $request->description;
			$expense->amount = $request->amount;
			$expense->ex_date = $request->ex_date;
			$expense->ex_date = date('Y-m-d');
			
			$expense->save ();
			DB::update("UPDATE accounts SET amount = amount - '$request->amount' where id = 1");
			DB::update("UPDATE accounts SET amount = amount + '$request->amount' where id = 4");
	    	return response()->json(['success'=>'Data is successfully added']); 
		}
    	   	
    }

     public function delete(Request $request){

     	// echo $request->id;die;
		$expense = Expense::findorfail($request->id);
		$expense->destroy($request->id);

		return response()->json(['success'=>'Data is successfully Deleted']);     	
    }
}
