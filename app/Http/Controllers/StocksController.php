<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Auth;
use DB;
use Session;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class StocksController extends Controller
{
    public function index(){
    	$products = DB::table('products')
						->select('*')
						->get();
    	return view('stock.list', compact('products'));
    }

    public function add(Request $request){

    	// echo $request->code.$request->name.$request->purchased.$request->gst.$request->quantity.$request->rate.$request->id;die;

		if (Product::where('id', $request->id)->count() > 0) {

			DB::update("UPDATE products SET code = '$request->code', name = '$request->name', gst = '$request->gst', quantity = '$request->quantity', rate = '$request->rate', ime_1 = '$request->ime_1', ime_2 = '$request->ime_2' WHERE id = ? ",[$request->id]);
    		return response()->json(['success'=>'Data is successfully Edited']);  
		} else {
			$product = new Product();
			$product->code = $request->code;
			$product->name = $request->name;
			$product->purchased = $request->purchased;
			$product->gst = $request->gst;
			$product->quantity = $request->quantity;
			$product->rate = $request->rate;
			$product->ime_1 = $request->ime_1;
			$product->ime_2 = $request->ime_2;
			
			$product->save ();
	    	return response()->json(['success'=>'Data is successfully added']); 
		}
    	   	
    }

     public function delete(Request $request){

     	// echo $request->id;die;
		$product = Product::findorfail($request->id);
		$product->destroy($request->id);

		return response()->json(['success'=>'Data is successfully Deleted']);     	
    }

    public function prodname(Request $request){
    	
		$json=array();

		$status = Product::where('quantity','>', 0)
						->where('name','like', '%' . $request->term . '%')
						->orWhere('ime_1','like', '%' . $request->term . '%')						
						
						->get();

		foreach ($status as $key => $value) {
				$val = $value->name."-*".$value->ime_1;
				$json[] =  $val;
			}

		echo json_encode($json);						
	}

	public function proddetails(Request $request){

		$str = (string) $request->value;
		// echo $str; die;
		$splitName = explode('-*', $str);
		// echo $splitName[0];
		// echo $splitName[1];die;
		$cust = Product::where('name',$splitName[0])
				->where('ime_1',$splitName[1])
				->where('quantity','>', 0)
				->get();

		foreach ($cust as $key => $value) {			
				$json['id'] =  $value->id;
				$json['code'] =  $value->code;		
				$json['rate'] =  $value->rate;
				$json['gst'] =  $value->gst;				
				$json['qty'] =  $value->quantity;		
			}

		echo json_encode($json);

	}
}
