<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\InvoiceNum;
use App\Models\Customer;
use App\Models\SalesEstimate;
use Auth;
use DB;
use Session;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class EstimateController extends Controller
{
    public function salesaddest(){
   
		$sales_bill = DB::table('sales_estimates')
						->select('*')
						->get();

		$sales_bill_temps = DB::table('sales_estimates')
						->select('*')
						->get();

    	return view('estimate.salesaddest', compact('sales_bill', 'sales_bill_temps'));
    }

    public function tempaddsales(Request $request){
    	// echo $request->cudate.$request->custname.$request->custphone.$request->prodname.$request->prodid.$request->item_rate.$request->item_qty.$request->item_total;die;
//    	$custid = $request->custid;
//    	if($request->custid == ''){
//    		$customer = new Customer();
//    		$customer->name = $request->custname;
//    		$customer->phone = $request->custphone;
//    		$customer->amount = 0;
//    		$customer->save();
//    		$custid = $customer->id;
//    	}

		$sales = new SalesEstimate();
		$sales->bill_date = $request->cudate;
        $sales->customer_name = $request->custname;
        $sales->customer_phone = $request->custphone;
        $sales->product_name = $request->prodname;
		$sales->product_id = $request->prodid;
		$sales->rate = $request->item_rate;
		$sales->quantity = $request->item_qty;
		$sales->total = $request->item_total;
		
		$sales->save ();

    	return response()->json(['success'=>'Data is successfully added']); 
    }

    public function tempdetailssales(Request $request){

		$sales_bill = DB::table('sales_estimates')
						->select('*')
						->get();

		$sales_bill_temps = DB::table('sales_estimates')
						->select('*')
						->get();

    	return view('estimate.salesdetailsest', compact('sales_bill', 'sales_bill_temps'));
    }
    public function tempdeletesales(){

    	SalesEstimate::truncate();

    	return redirect('estimate/sales');
    }

    public function tempdeletedatasales(Request $request){

     	// echo $request->id;die;
		$product = SalesEstimate::findorfail($request->id);
		$product->destroy($request->id);

		return response()->json(['success'=>'Data is successfully Deleted']);     	
    }

    public function salesaddmainest(Request $request){

		SalesEstimate::truncate();

    	return redirect('estimate/sales');
    }
}
