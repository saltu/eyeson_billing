<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\InvoiceNum;
use App\Models\Customer;
use App\Models\SalesBill;
use App\Models\SalesBillDetail;
use App\Models\SalesBillTemp;
use App\Models\SalesPayment;
use Auth;
use DB;
use Session;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class SalesController extends Controller
{
    public function salesadd(){
    	$invoice = DB::table('invoice_nums')
						->select('invoice_num')
						->where('id','1')
						->where('name','Sales')
						->get();
		$innum = $invoice[0]->invoice_num;

		$sales_bill = DB::table('sales_bill_temps')
						->select('*')
						->get();

		$sales_bill_temps = DB::table('sales_bill_temps')
						->select('*')
						->get();

    	return view('sales.salesadd', compact('innum', 'sales_bill', 'sales_bill_temps'));
    }

    public function tempadd(Request $request){
    	// echo $request->billno.$request->custid.$request->prodid.$request->item_qty.$request->item_dis.$request->item_cgst.$request->item_sgst.$request->item_total;die;
    	$custid = $request->custid;
    	if($request->custid == ''){
    		$customer = new Customer();
    		$customer->name = $request->custname;
    		$customer->phone = $request->custphone;
    		$customer->amount = 0;
    		$customer->save();
    		$custid = $customer->id;
    	}

		$sales = new SalesBillTemp();
		$sales->bill_date = $request->cudate;
		$sales->bill_no = $request->billno;
		$sales->customer_id = $custid;
		$sales->product_id = $request->prodid;
		$sales->rate = $request->item_rate;
		$sales->quantity = $request->item_qty;
		$sales->cgst = $request->item_cgst;
		$sales->sgst = $request->item_sgst;
        $sales->cess = $request->item_cess;
		$sales->total = $request->item_total;
		
		$sales->save ();

    	return response()->json(['success'=>'Data is successfully added']); 
    }

    public function tempdetails(Request $request){

		$sales_bill = DB::table('sales_bill_temps')
						->select('*')
						->get();

		$sales_billss = DB::table('sales_bill_temps')
						->select('*')
						->get();

    	return view('sales.salesdetails', compact('sales_bill', 'sales_billss'));
    }
    public function tempdelete(){

    	SalesBillTemp::truncate();

    	return redirect('sales/salesadd');
    }

    public function tempdeletedata(Request $request){

     	// echo $request->id;die;
		$product = SalesBillTemp::findorfail($request->id);
		$product->destroy($request->id);

		return response()->json(['success'=>'Data is successfully Deleted']);     	
    }

    public function salesaddmain(Request $request){

    	$tot = 0;
    	$tcgst = 0;
    	$tsgst = 0;
        $tcess = 0;
    	$bill_date = 0;
    	$bill_no = 0;
    	$customer_id = 0;
    	$amountpay = $request->amountpay;
    	$mode = $_POST['smode'];

    	DB::update("UPDATE sales_bill_temps SET amountpay = '$request->amountpay'");

    	$sales_bill_temps = DB::table('sales_bill_temps')
						->select('*')
						->get();

		foreach ($sales_bill_temps as $key => $value) {
			$tot += $value->total;
			$tcgst += $value->cgst;
			$tsgst += $value->sgst;
            $tcess += $value->cess;
			$bill_date = $value->bill_date;
	    	$bill_no = $value->bill_no;
	    	$customer_id = $value->customer_id;
		}

		$salesdet = new SalesBillDetail();
		$salesdet->bill_date = $bill_date;
		$salesdet->bill_no = $bill_no;
		$salesdet->customer_id = $customer_id;
		$salesdet->tcgst = $tcgst;
		$salesdet->tsgst = $tsgst;
        $salesdet->tcess = $tcess;
		$salesdet->total = $tot;
		$salesdet->amountpay = $amountpay;
		$salesdet->amountpayl = 0;
		$salesdet->save ();

		foreach ($sales_bill_temps as $key => $value) {
			$sales = new SalesBill();
			$sales->bill_id = $salesdet->id;
			$sales->bill_no = $value->bill_no;
			$sales->product_id = $value->product_id;
			$sales->rate = $value->rate;
			$sales->quantity = $value->quantity;
			$sales->total = $value->total;

			DB::update("UPDATE products SET quantity = quantity - '$value->quantity'  where id = '$value->product_id'");
			$sales->save ();
		}
		if($mode == 'cash'){
			$salespayment = new SalesPayment();
			$salespayment->billno = $salesdet->bill_no;
			$salespayment->desc = "Bill";
			$salespayment->billdate = $salesdet->bill_date;
			$salespayment->amount = $request->amountpay;
			$salespayment->mode = "cash";
			$salespayment->ref = "cash";
			$salespayment->save ();
		} else if($mode == 'bank') {
			$salespayment = new SalesPayment();
			$salespayment->billno = $salesdet->bill_no;
			$salespayment->desc = "Bill";
			$salespayment->billdate = $salesdet->bill_date;
			$salespayment->amount = $request->amountpay;
			$salespayment->mode = "bank";
			$salespayment->ref = $request->sref;
			$salespayment->save ();
		}

		$amnt = $tot - $request->amountpay;

		DB::update("UPDATE invoice_nums SET invoice_num = invoice_num + 1 where id = 1");
		DB::update("UPDATE accounts SET amount = amount + '$request->amountpay' where id = 1");
		DB::update("UPDATE accounts SET amount = amount + '$request->amountpay' where id = 2");

		DB::update("UPDATE customers SET amount = amount + '$amnt' where id = '$customer_id'");
		SalesBillTemp::truncate();

    	return redirect('sales/salesadd');
    }

    public function salesreport(){
    	return view('sales.salesreport');
    }

    public function salesreportdata(Request $request){
		// echo $request->datefrom.$request->dateto;die;
		$datefrom = $request->datefrom;
		$dateto = $request->dateto;

		$sales_bill_data = DB::table('sales_bill_details')
										->select('*')
										->whereBetween('bill_date',[$datefrom,$dateto])
										->groupBy('bill_no')
										->orderBy('id','DESC')
										->get();

		return view('sales.salesreportlist', compact('sales_bill_data'));
	}
	public function salesreportdetails($billno){
			// echo $billno;die;

		$sales_bill = DB::table('sales_bill_details')
							->select('*')
							->where('bill_no',$billno)
							->get();

		$sales_billss = DB::table('sales_bills')
							->select('*')
							->where('bill_no',$billno)
							->get();

		return view('sales.salesreportdetails', compact('sales_bill', 'sales_billss'));
	}

    public function salesentry(){
    	return view('sales.salesentry');
    }
   
}
