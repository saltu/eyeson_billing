<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SalesBillDetail;
use App\Models\SalesPayment;
use Auth;
use DB;
use Session;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class SalesPaymentController extends Controller
{
    public function index(){
    	return view('payment.sales');
    }

    public function billno(Request $request){
    	
		$json=array();

		$status = SalesBillDetail::where('bill_no','like', '%' . $request->term . '%')
						->groupBy('bill_no')
						->get();

		foreach ($status as $key => $value) {
				$json[] =  $value->bill_no;
			}

		echo json_encode($json);						
	}

	public function billdetails(Request $request){
		$bill = SalesBillDetail::where('bill_no',[$request->value])
				->get();
		
		$tot = 0;

		foreach ($bill as $key => $value) {
				$cutname = DB::table('customers')
						->select('name')
						->where('id', $value->customer_id)	
						->get();

				$tot +=  $value->total;	
				$json['amountpay'] =  $value->amountpay;
				$json['amountpayl'] =  $value->amountpayl;
				$json['custid'] = $value->customer_id;
			}

		$json['custname'] =  $cutname[0]->name;
		$json['total'] =  round($tot);

		echo json_encode($json);
	}

	public function billadd(Request $request){

    	// echo $request->code.$request->name.$request->purchased.$request->gst.$request->quantity.$request->rate;die;
		$salespayment = new SalesPayment();
		$salespayment->billno = $request->psbillno;
		$salespayment->desc = "Payment";
		$salespayment->billdate = date('y-m-d');
		$salespayment->amount = $request->psalespay;
		$salespayment->mode = "cash";
		$salespayment->ref = "cash";
		$salespayment->save ();

		DB::update("UPDATE sales_bill_details SET amountpayl = amountpayl + '$request->psalespay' where bill_no = '$request->psbillno'");

		DB::update("UPDATE accounts SET amount = amount + '$request->psalespay' where id = 1");
		DB::update("UPDATE accounts SET amount = amount + '$request->psalespay' where id = 2");

		DB::update("UPDATE customers SET amount = amount + '$request->psalespay' where id = '$request->pcustid'");

    	return redirect('home');
    	   	
    }

    public function customer(){
    	return view('payment.customer');
    }

    public function custbillno(Request $request){
    	// echo $request->value;die;

    	$json=array();

		$status = SalesBillDetail::where('customer_id',$request->value)
						->groupBy('bill_no')
						->get();

		foreach ($status as $key => $value) {
				$json[] =  $value->bill_no;
			}

		echo json_encode($json);
    }

}
