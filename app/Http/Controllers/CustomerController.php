<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use Auth;
use DB;
use Session;
use Hash;
use Redirect;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;


class CustomerController extends Controller
{
    public function index(){
    	$customers = DB::table('customers')
						->select('*')
						->get();
    	return view('customer.list', compact('customers'));
    }

    public function add(Request $request){

    	// echo $request->code.$request->name.$request->purchased.$request->gst.$request->quantity.$request->rate;die;

		if (Customer::where('id', $request->id)->count() > 0) {
			DB::update("UPDATE customers SET name = '$request->name', phone = '$request->phone' WHERE id = ? ",[$request->id]);
    		return response()->json(['success'=>'Data is successfully Edited']);  
		} else {
			$customer = new Customer();
			$customer->name = $request->name;
			$customer->phone = $request->phone;
			$customer->amount = 0;
			
			$customer->save ();
	    	return response()->json(['success'=>'Data is successfully added']); 
		}
    	   	
    }

     public function delete(Request $request){

     	// echo $request->id;die;
		$customers = Customer::findorfail($request->id);
		$customers->destroy($request->id);

		return response()->json(['success'=>'Data is successfully Deleted']);     	
    }

    public function custname(Request $request){
    	
		$json=array();

		$status = Customer::where('name','like', '%' . $request->term . '%')
						->get();

		foreach ($status as $key => $value) {
				$json[] =  $value->name;
			}

		echo json_encode($json);						
	}

	public function custdetails(Request $request){
		$cust = Customer::where('name',[$request->value])
				->get();

		foreach ($cust as $key => $value) {
				$json['id'] =  $value->id;		
				$json['phone'] =  $value->phone;
			}

		echo json_encode($json);

	}
    
}
