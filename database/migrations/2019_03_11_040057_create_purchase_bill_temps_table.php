<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseBillTempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_bill_temps', function (Blueprint $table) {
            $table->increments('id');
            $table->date('bill_date')->nullable();
            $table->string('bill_no')->nullable();
            $table->string('purbillno')->nullable();
            $table->date('purbilldate')->nullable();
            $table->integer('purchaser_id')->nullable();
            $table->string('product_code')->nullable();
            $table->string('product_name')->nullable();
            $table->float('cgstp')->nullable();
            $table->float('sgstp')->nullable();
            $table->float('product_rate')->nullable();
            $table->string('ime_1')->nullable();
            $table->string('ime_2')->nullable();
            $table->float('cgst')->nullable();
            $table->float('sgst')->nullable();
            $table->float('quantity')->nullable();
            $table->float('discount')->nullable();
            $table->float('total')->nullable();
            $table->float('amountpay')->nullable();
            $table->float('amountpayl')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_bill_temps');
    }
}
