<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseBillDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_bill_details', function (Blueprint $table) {
            $table->increments('id');
            $table->date('bill_date')->nullable();
            $table->string('bill_no')->nullable();
            $table->string('purbillno')->nullable();
            $table->date('purbilldate')->nullable();
            $table->integer('purchaser_id')->nullable();
            $table->float('tcgst')->nullable();
            $table->float('tsgst')->nullable();
            $table->float('discount')->nullable();
            $table->float('total')->nullable();
            $table->float('amountpay')->nullable();
            $table->float('amountpayl')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_bill_details');
    }
}
