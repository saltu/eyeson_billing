<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesBillTempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_bill_temps', function (Blueprint $table) {
            $table->increments('id');
            $table->date('bill_date')->nullable();
            $table->string('bill_no')->nullable();
            $table->integer('customer_id')->nullable();
            $table->integer('product_id')->nullable();
            $table->float('rate')->nullable();
            $table->float('cgst')->nullable();
            $table->float('sgst')->nullable();            
            $table->float('cess')->nullable();
            $table->float('quantity')->nullable();
            $table->float('total')->nullable();
            $table->float('amountpay')->nullable();
            $table->float('amountpayl')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_bill_temps');
    }
}
