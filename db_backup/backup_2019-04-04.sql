CREATE TABLE `accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `expenses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `ex_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `invoice_nums` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_num` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `open_closes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `current_date` date DEFAULT NULL,
  `opening` double DEFAULT NULL,
  `closing` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchased` int(11) DEFAULT NULL,
  `gst` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `ime_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ime_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `purchase_bill_temps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bill_date` date DEFAULT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purbillno` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purbilldate` date DEFAULT NULL,
  `purchaser_id` int(11) DEFAULT NULL,
  `product_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgstp` float DEFAULT NULL,
  `sgstp` float DEFAULT NULL,
  `product_rate` float DEFAULT NULL,
  `ime_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ime_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgst` double(8,2) DEFAULT NULL,
  `sgst` double(8,2) DEFAULT NULL,
  `quantity` double(8,2) DEFAULT NULL,
  `discount` double(8,2) DEFAULT NULL,
  `total` double(8,2) DEFAULT NULL,
  `amountpay` double(8,2) DEFAULT NULL,
  `amountpayl` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `purchase_bills` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bill_date` date DEFAULT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purbillno` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purbilldate` date DEFAULT NULL,
  `purchaser_id` int(11) DEFAULT NULL,
  `product_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgstp` float DEFAULT NULL,
  `sgstp` float DEFAULT NULL,
  `product_rate` float DEFAULT NULL,
  `ime_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ime_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgst` double(8,2) DEFAULT NULL,
  `sgst` double(8,2) DEFAULT NULL,
  `quantity` double(8,2) DEFAULT NULL,
  `discount` double(8,2) DEFAULT NULL,
  `total` double(8,2) DEFAULT NULL,
  `amountpay` double(8,2) DEFAULT NULL,
  `amountpayl` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `purchase_payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `billno` int(11) DEFAULT NULL,
  `billdate` date DEFAULT NULL,
  `amount` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `purchasers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(8,2) DEFAULT NULL,
  `gst` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `sales_bill_temps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bill_date` date DEFAULT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `cgst` float DEFAULT NULL,
  `sgst` float DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  `amountpay` float DEFAULT NULL,
  `amountpayl` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `sales_bills` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bill_date` date DEFAULT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `cgst` float DEFAULT NULL,
  `sgst` float DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  `amountpay` float DEFAULT NULL,
  `amountpayl` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `sales_payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `billno` int(11) DEFAULT NULL,
  `billdate` date DEFAULT NULL,
  `amount` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 
 
INSERT INTO `accounts` ( `id`, `name`, `amount`, `created_at`, `updated_at`) VALUES 
('1', 'Account', '56914', '', ''), 
('2', 'Sales', '61500', '', ''), 
('3', 'Purchase', '5209', '', ''), 
('4', 'Expense', '121', '', '');  


INSERT INTO `customers` ( `id`, `name`, `phone`, `amount`, `created_at`, `updated_at`) VALUES 
('1', 'dxc', 'sdczsdfsd', '', '2019-03-08 06:06:53', '2019-03-08 06:06:53'), 
('2', 'NIthn', '9847296004', '', '2019-03-08 06:08:33', '2019-03-08 06:08:33'), 
('3', '', '', '', '2019-03-12 04:44:52', '2019-03-12 04:44:52'), 
('4', '', '', '', '2019-03-12 04:46:28', '2019-03-12 04:46:28'), 
('5', '', '', '', '2019-03-12 04:49:03', '2019-03-12 04:49:03'), 
('6', 'sd', 'dsf', '', '2019-03-12 04:51:11', '2019-03-12 04:51:11');  


INSERT INTO `expenses` ( `id`, `description`, `amount`, `ex_date`, `created_at`, `updated_at`) VALUES 
('1', 'Tea', '100', '2019-03-06', '2019-03-06 12:01:57', '2019-03-06 12:01:57'), 
('2', 'zx', '12', '2019-03-12', '2019-03-12 17:04:25', '2019-03-12 17:04:25'), 
('3', 'scx', '121', '2019-03-12', '2019-03-12 17:05:30', '2019-03-12 17:05:30');  


INSERT INTO `invoice_nums` ( `id`, `name`, `invoice_num`, `created_at`, `updated_at`) VALUES 
('1', 'Sales', '5', '', ''), 
('2', 'Purchase', '7', '', '');  


INSERT INTO `migrations` ( `id`, `migration`, `batch`) VALUES 
('1', '2014_10_12_000000_create_users_table', '1'), 
('2', '2014_10_12_100000_create_password_resets_table', '1'), 
('3', '2019_03_04_053630_create_products_table', '2'), 
('4', '2019_03_04_070900_create_expenses_table', '3'), 
('5', '2019_03_04_102935_create_invoice_nums_table', '4'), 
('6', '2019_03_04_070830_create_sales_bills_table', '5'), 
('7', '2019_03_08_053700_create_customers_table', '5'), 
('8', '2019_03_08_053806_create_sales_bill_temps_table', '5'), 
('9', '2019_03_09_053954_create_sales_payments_table', '6'), 
('10', '2019_03_04_070954_create_accounts_table', '7'), 
('11', '2019_03_11_040031_create_purchase_payments_table', '8'), 
('12', '2019_03_11_040048_create_purchase_bills_table', '8'), 
('13', '2019_03_11_040057_create_purchase_bill_temps_table', '8'), 
('14', '2019_03_11_040118_create_purchasers_table', '8'), 
('15', '2019_03_04_071041_create_open_closes_table', '9');  


INSERT INTO `open_closes` ( `id`, `current_date`, `opening`, `closing`, `created_at`, `updated_at`) VALUES 
('1', '2019-03-12', '56914', '', '2019-03-25 07:08:51', '2019-03-25 07:08:51'), 
('2', '2019-03-28', '56914', '', '2019-03-28 05:04:54', '2019-03-28 05:04:54'), 
('3', '2019-03-30', '56914', '', '2019-03-30 06:24:31', '2019-03-30 06:24:31'), 
('4', '2019-04-02', '56914', '', '2019-04-02 05:24:13', '2019-04-02 05:24:13'), 
('5', '2019-04-03', '56914', '', '2019-04-03 04:49:27', '2019-04-03 04:49:27'), 
('6', '2019-04-04', '56914', '', '2019-04-04 04:46:44', '2019-04-04 04:46:44');  


 


INSERT INTO `products` ( `id`, `code`, `name`, `purchased`, `gst`, `quantity`, `rate`, `ime_1`, `ime_2`, `created_at`, `updated_at`) VALUES 
('1', '123', 'iphone', '2', '6', '8', '11555', '12345678901111111111', '', '2019-03-09 07:07:05', '2019-03-09 07:07:05'), 
('2', '321', 'oppo', '2', '6', '1', '10000', '9876543210', '', '2019-03-09 07:07:29', '2019-03-09 07:07:29'), 
('3', 'sd', 'sd', '2', '2', '1', '1', 'sd', '', '2019-03-11 08:40:44', '2019-03-11 08:40:44'), 
('4', 'aaa', 'aa', '2', '12', '1', '1000', 'aa', '', '2019-03-11 10:17:57', '2019-03-11 10:17:57'), 
('5', 'sdf', 'sdf', '2', '12', '1', '1111', 'sdf', '', '2019-03-12 17:09:58', '2019-03-12 17:09:58'), 
('6', 'sdfx', 'sdf', '2', '12', '1', '12', 'xvc', '', '2019-03-12 17:09:58', '2019-03-12 17:09:58'), 
('7', 'sdf', 'sdf', '2', '2', '1', '1', 'sdf', '', '2019-03-12 17:09:58', '2019-03-12 17:09:58'), 
('8', '1ewds1', '12e', '2', '2', '11', '1', 'ds', '', '2019-03-12 17:09:58', '2019-03-12 17:09:58'), 
('9', '1', 'dcx', '2', '2', '1', '1', 'cx', '', '2019-03-12 17:09:58', '2019-03-12 17:09:58'), 
('10', 'd', 'sdf', '', '12', '12', '121', 'werwer', '', '2019-03-14 05:06:21', '2019-03-14 05:06:21'), 
('11', 'cxv', 'sf', '2', '12', '1', '1212', '0', '', '2019-03-15 11:45:15', '2019-03-15 11:45:15'), 
('12', 'asd', 'asd', '2', '10', '1', '123', 'asd', '', '2019-03-15 11:48:18', '2019-03-15 11:48:18'), 
('13', 'asd', 'asd', '2', '10', '1', '123', 'asd', '', '2019-03-15 11:48:18', '2019-03-15 11:48:18'), 
('14', 'asd', 'asd', '2', '10', '1', '123', 'asd', '', '2019-03-15 11:48:18', '2019-03-15 11:48:18'), 
('15', 'asd', 'asd', '2', '10', '1', '123', 'asd', '', '2019-03-15 11:48:18', '2019-03-15 11:48:18'), 
('16', 'sd', 'zscv', '4', '10', '1', '12', 'zsd', '', '2019-03-15 11:58:38', '2019-03-15 11:58:38');  


 


INSERT INTO `purchase_bills` ( `id`, `bill_date`, `bill_no`, `purbillno`, `purbilldate`, `purchaser_id`, `product_code`, `product_name`, `cgstp`, `sgstp`, `product_rate`, `ime_1`, `ime_2`, `cgst`, `sgst`, `quantity`, `discount`, `total`, `amountpay`, `amountpayl`, `created_at`, `updated_at`) VALUES 
('1', '2019-03-11', '1', '', '', '2', 'sd', 'sd', '1', '1', '1', 'sd', '', '0.00', '0.00', '1.00', '1.00', '0.99', '11.00', '0.00', '2019-03-11 08:40:44', '2019-03-11 08:40:44'), 
('2', '2019-03-11', '2', '', '', '2', 'aaa', 'aa', '6', '6', '1000', 'aa', '', '60.00', '60.00', '1.00', '', '1120.00', '240.00', '0.00', '2019-03-11 10:17:57', '2019-03-11 10:17:57'), 
('3', '2019-03-12', '3', '323we', '2019-03-14', '2', 'sdf', 'sdf', '6', '6', '1111', 'sdf', '', '66.66', '66.66', '1.00', '', '1244.32', '0.00', '0.00', '2019-03-12 17:09:58', '2019-03-12 17:09:58'), 
('4', '2019-03-12', '3', '323we', '2019-03-14', '2', 'sdfx', 'sdf', '6', '6', '12', 'xvc', '', '0.72', '0.72', '1.00', '', '13.44', '0.00', '0.00', '2019-03-12 17:09:58', '2019-03-12 17:09:58'), 
('5', '2019-03-12', '3', '323we', '2019-03-14', '2', 'sdf', 'sdf', '1', '1', '1', 'sdf', '', '0.01', '0.01', '1.00', '', '1.02', '0.00', '0.00', '2019-03-12 17:09:58', '2019-03-12 17:09:58'), 
('6', '2019-03-12', '3', '323we', '2019-03-14', '2', '1ewds1', '12e', '1', '1', '1', 'ds', '', '0.11', '0.11', '11.00', '', '11.22', '0.00', '0.00', '2019-03-12 17:09:58', '2019-03-12 17:09:58'), 
('7', '2019-03-12', '3', '323we', '2019-03-14', '2', '1', 'dcx', '1', '1', '1', 'cx', '', '0.01', '0.01', '1.00', '', '1.02', '0.00', '0.00', '2019-03-12 17:09:58', '2019-03-12 17:09:58'), 
('8', '2019-03-15', '4', 'trt', '2019-03-15', '2', 'cxv', 'sf', '6', '6', '1212', '0', '', '290.88', '290.88', '4.00', '', '5429.76', '2323.00', '0.00', '2019-03-15 11:45:15', '2019-03-15 11:45:15'), 
('9', '2019-03-15', '5', 'sdz', '2019-03-06', '2', 'asd', 'asd', '5', '5', '123', 'asd', '', '24.60', '24.60', '4.00', '', '541.20', '212.00', '0.00', '2019-03-15 11:48:18', '2019-03-15 11:48:18'), 
('10', '2019-03-15', '6', 'sdf', '0004-04-23', '4', 'sd', 'zscv', '5', '5', '12', 'zsd', '', '0.60', '0.60', '1.00', '', '13.20', '2323.00', '100.00', '2019-03-15 11:58:38', '2019-03-15 11:58:38');  


INSERT INTO `purchase_payments` ( `id`, `billno`, `billdate`, `amount`, `created_at`, `updated_at`) VALUES 
('1', '1', '2019-03-11', '11.00', '2019-03-11 08:40:44', '2019-03-11 08:40:44'), 
('2', '2', '2019-03-11', '240.00', '2019-03-11 10:17:57', '2019-03-11 10:17:57'), 
('3', '3', '2019-03-12', '0.00', '2019-03-12 17:09:58', '2019-03-12 17:09:58'), 
('4', '4', '2019-03-15', '2323.00', '2019-03-15 11:45:16', '2019-03-15 11:45:16'), 
('5', '5', '2019-03-15', '212.00', '2019-03-15 11:48:18', '2019-03-15 11:48:18'), 
('6', '6', '2019-03-15', '2323.00', '2019-03-15 11:58:38', '2019-03-15 11:58:38');  


INSERT INTO `purchasers` ( `id`, `name`, `phone`, `amount`, `gst`, `created_at`, `updated_at`) VALUES 
('2', 'Nitin', '6985741231', '0.00', 'sdrgsdrx', '2019-03-11 05:36:58', '2019-03-11 05:36:58'), 
('3', 'sfd', 'sdxc', '0.00', 'rdsf', '2019-03-12 04:39:54', '2019-03-12 04:39:54'), 
('4', 'sdf', 'sdf', '100.00', 'werwe', '2019-03-15 11:56:45', '2019-03-15 11:56:45');  


INSERT INTO `sales_bill_temps` ( `id`, `bill_date`, `bill_no`, `customer_id`, `product_id`, `rate`, `cgst`, `sgst`, `quantity`, `discount`, `total`, `amountpay`, `amountpayl`, `created_at`, `updated_at`) VALUES 
('1', '2019-04-03', '5', '2', '1', '14100', '450', '450', '1', '', '15000', '', '', '2019-04-03 07:23:51', '2019-04-03 07:23:51');  


INSERT INTO `sales_bills` ( `id`, `bill_date`, `bill_no`, `customer_id`, `product_id`, `rate`, `cgst`, `sgst`, `quantity`, `discount`, `total`, `amountpay`, `amountpayl`, `created_at`, `updated_at`) VALUES 
('1', '2019-03-09', '1', '2', '1', '', '300', '300', '1', '0', '10600', '11200', '200', '2019-03-09 07:08:26', '2019-03-09 07:08:26'), 
('2', '2019-03-09', '2', '2', '2', '', '600', '600', '2', '0', '21200', '22400', '100', '2019-03-09 07:59:44', '2019-03-09 07:59:44'), 
('3', '2019-03-12', '3', '2', '1', '', '300', '300', '1', '0', '10600', '11200', '0', '2019-03-12 17:28:02', '2019-03-12 17:28:02'), 
('4', '2019-03-02', '4', '2', '1', '18800', '600', '600', '1', '', '20000', '30000', '20000', '2019-03-22 11:37:08', '2019-03-22 11:37:08'), 
('5', '2019-03-02', '4', '2', '1', '18800', '1200', '1200', '2', '', '40000', '30000', '20000', '2019-03-22 11:37:08', '2019-03-22 11:37:08');  


INSERT INTO `sales_payments` ( `id`, `billno`, `billdate`, `amount`, `created_at`, `updated_at`) VALUES 
('1', '3', '2019-03-12', '11200.00', '2019-03-12 17:28:02', '2019-03-12 17:28:02'), 
('2', '4', '2019-03-02', '30000.00', '2019-03-22 11:37:08', '2019-03-22 11:37:08');  


INSERT INTO `users` ( `id`, `name`, `username`, `role`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES 
('1', 'Admin', 'admin', '0', '$2y$10$HjlwUNi/s8bpXh09PCZ68OIQAnD4h4ykpEQACDw0WrEIW3tFZll1W', '', '2019-02-27 11:04:45', '2019-02-27 11:04:45'); 