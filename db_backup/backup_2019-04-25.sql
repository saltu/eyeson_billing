CREATE TABLE `accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `expenses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `ex_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `invoice_nums` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_num` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `open_closes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `current_date` date DEFAULT NULL,
  `opening` double DEFAULT NULL,
  `closing` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchased` int(11) DEFAULT NULL,
  `gst` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `ime_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ime_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `purchase_bill_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bill_date` date DEFAULT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purbillno` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purbilldate` date DEFAULT NULL,
  `purchaser_id` int(11) DEFAULT NULL,
  `tcgst` double(8,2) DEFAULT NULL,
  `tsgst` double(8,2) DEFAULT NULL,
  `discount` double(8,2) DEFAULT NULL,
  `total` double(8,2) DEFAULT NULL,
  `amountpay` double(8,2) DEFAULT NULL,
  `amountpayl` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `purchase_bill_temps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bill_date` date DEFAULT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purbillno` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purbilldate` date DEFAULT NULL,
  `purchaser_id` int(11) DEFAULT NULL,
  `product_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgstp` float DEFAULT NULL,
  `sgstp` float DEFAULT NULL,
  `product_rate` float DEFAULT NULL,
  `ime_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ime_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgst` double(8,2) DEFAULT NULL,
  `sgst` double(8,2) DEFAULT NULL,
  `quantity` double(8,2) DEFAULT NULL,
  `discount` double(8,2) DEFAULT NULL,
  `total` double(8,2) DEFAULT NULL,
  `amountpay` double(8,2) DEFAULT NULL,
  `amountpayl` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `purchase_bills` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bill_id` int(11) DEFAULT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgstp` double(8,2) DEFAULT NULL,
  `sgstp` double(8,2) DEFAULT NULL,
  `product_rate` double(8,2) DEFAULT NULL,
  `ime_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ime_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgst` double(8,2) DEFAULT NULL,
  `sgst` double(8,2) DEFAULT NULL,
  `quantity` double(8,2) DEFAULT NULL,
  `total` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `purchase_payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `billno` int(11) DEFAULT NULL,
  `desc` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billdate` date DEFAULT NULL,
  `mode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `purchasers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(8,2) DEFAULT NULL,
  `gst` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `sales_bill_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bill_date` date DEFAULT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `tcgst` double(8,2) DEFAULT NULL,
  `tsgst` double(8,2) DEFAULT NULL,
  `total` double(8,2) DEFAULT NULL,
  `amountpay` double(8,2) DEFAULT NULL,
  `amountpayl` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `sales_bill_temps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bill_date` date DEFAULT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `cgst` float DEFAULT NULL,
  `sgst` float DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  `amountpay` float DEFAULT NULL,
  `amountpayl` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `sales_bills` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bill_id` int(11) DEFAULT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `rate` double(8,2) DEFAULT NULL,
  `quantity` double(8,2) DEFAULT NULL,
  `total` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `sales_estimates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bill_date` date DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `rate` double(8,2) DEFAULT NULL,
  `quantity` double(8,2) DEFAULT NULL,
  `total` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `sales_payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `billno` int(11) DEFAULT NULL,
  `desc` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billdate` date DEFAULT NULL,
  `mode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci; 
 
INSERT INTO `accounts` ( `id`, `name`, `amount`, `created_at`, `updated_at`) VALUES 
('1', 'Account', '199039', '', ''), 
('2', 'Sales', '301800', '', ''), 
('3', 'Purchase', '103084', '', ''), 
('4', 'Expense', '421', '', '');  


INSERT INTO `customers` ( `id`, `name`, `phone`, `amount`, `created_at`, `updated_at`) VALUES 
('1', 'dxc', '9847296004', '', '2019-03-08 06:06:53', '2019-03-08 06:06:53'), 
('2', 'NIthn', '9847296004', '', '2019-03-08 06:08:33', '2019-03-08 06:08:33'), 
('6', 'sd', '9847296004', '', '2019-03-12 04:51:11', '2019-03-12 04:51:11');  


INSERT INTO `expenses` ( `id`, `description`, `amount`, `ex_date`, `created_at`, `updated_at`) VALUES 
('1', 'Tea', '100', '2019-03-06', '2019-03-06 12:01:57', '2019-03-06 12:01:57'), 
('2', 'zx', '12', '2019-03-12', '2019-03-12 17:04:25', '2019-03-12 17:04:25'), 
('3', 'scx', '121', '2019-03-12', '2019-03-12 17:05:30', '2019-03-12 17:05:30'), 
('4', 'Tea', '100', '2019-04-11', '2019-04-11 05:28:39', '2019-04-11 05:28:39'), 
('5', 'paper', '200', '2019-04-11', '2019-04-11 05:28:50', '2019-04-11 05:28:50');  


INSERT INTO `invoice_nums` ( `id`, `name`, `invoice_num`, `created_at`, `updated_at`) VALUES 
('1', 'Sales', '15', '', ''), 
('2', 'Purchase', '13', '', '');  


INSERT INTO `migrations` ( `id`, `migration`, `batch`) VALUES 
('1', '2014_10_12_000000_create_users_table', '1'), 
('2', '2014_10_12_100000_create_password_resets_table', '1'), 
('3', '2019_03_04_053630_create_products_table', '2'), 
('4', '2019_03_04_070900_create_expenses_table', '3'), 
('5', '2019_03_04_102935_create_invoice_nums_table', '4'), 
('19', '2019_04_18_072235_create_sales_bill_details_table', '10'), 
('7', '2019_03_08_053700_create_customers_table', '5'), 
('8', '2019_03_08_053806_create_sales_bill_temps_table', '5'), 
('9', '2019_03_09_053954_create_sales_payments_table', '6'), 
('10', '2019_03_04_070954_create_accounts_table', '7'), 
('11', '2019_03_11_040031_create_purchase_payments_table', '8'), 
('20', '2019_03_11_040048_create_purchase_bills_table', '11'), 
('13', '2019_03_11_040057_create_purchase_bill_temps_table', '8'), 
('14', '2019_03_11_040118_create_purchasers_table', '8'), 
('15', '2019_03_04_071041_create_open_closes_table', '9'), 
('18', '2019_03_04_070830_create_sales_bills_table', '10'), 
('21', '2019_04_18_085048_create_purchase_bill_details_table', '11'), 
('22', '2019_04_23_054853_create_sales_estimates_table', '12');  


INSERT INTO `open_closes` ( `id`, `current_date`, `opening`, `closing`, `created_at`, `updated_at`) VALUES 
('1', '2019-03-12', '56914', '', '2019-03-25 07:08:51', '2019-03-25 07:08:51'), 
('2', '2019-03-28', '56914', '', '2019-03-28 05:04:54', '2019-03-28 05:04:54'), 
('3', '2019-03-30', '56914', '', '2019-03-30 06:24:31', '2019-03-30 06:24:31'), 
('4', '2019-04-02', '56914', '', '2019-04-02 05:24:13', '2019-04-02 05:24:13'), 
('5', '2019-04-03', '56914', '', '2019-04-03 04:49:27', '2019-04-03 04:49:27'), 
('6', '2019-04-04', '56914', '', '2019-04-04 04:46:44', '2019-04-04 04:46:44'), 
('7', '2019-04-05', '56914', '', '2019-04-05 04:57:38', '2019-04-05 04:57:38'), 
('8', '2019-04-06', '56914', '', '2019-04-06 07:50:43', '2019-04-06 07:50:43'), 
('9', '2019-04-11', '56914', '', '2019-04-11 05:11:40', '2019-04-11 05:11:40'), 
('10', '2019-04-12', '96730', '', '2019-04-12 05:24:37', '2019-04-12 05:24:37'), 
('11', '2019-04-16', '96730', '', '2019-04-16 05:42:28', '2019-04-16 05:42:28'), 
('12', '2019-04-17', '73597', '', '2019-04-17 04:48:35', '2019-04-17 04:48:35'), 
('13', '2019-04-18', '73597', '', '2019-04-18 05:58:10', '2019-04-18 05:58:10'), 
('14', '2019-04-20', '181117', '', '2019-04-20 04:47:42', '2019-04-20 04:47:42'), 
('15', '2019-04-22', '216117', '', '2019-04-22 04:57:49', '2019-04-22 04:57:49'), 
('16', '2019-04-23', '216117', '', '2019-04-23 04:30:55', '2019-04-23 04:30:55'), 
('17', '2019-04-24', '216117', '', '2019-04-24 04:57:48', '2019-04-24 04:57:48'), 
('18', '2019-04-25', '204339', '', '2019-04-25 06:40:12', '2019-04-25 06:40:12');  


 


INSERT INTO `products` ( `id`, `code`, `name`, `purchased`, `gst`, `quantity`, `rate`, `ime_1`, `ime_2`, `created_at`, `updated_at`) VALUES 
('1', '123', 'iphone', '2', '6', '7', '11555', '12345678901111111111', '', '2019-03-09 07:07:05', '2019-03-09 07:07:05'), 
('2', '321', 'oppo', '2', '6', '7', '10000', '9876543210', '', '2019-03-09 07:07:29', '2019-03-09 07:07:29'), 
('3', 'sd', 'sd', '2', '2', '1', '1', 'sd', '', '2019-03-11 08:40:44', '2019-03-11 08:40:44'), 
('4', 'aaa', 'aa', '2', '12', '1', '1000', 'aa', '', '2019-03-11 10:17:57', '2019-03-11 10:17:57'), 
('5', 'sdf', 'sdf', '2', '12', '1', '1111', 'sdf', '', '2019-03-12 17:09:58', '2019-03-12 17:09:58'), 
('6', 'sdfx', 'sdf', '2', '12', '1', '12', 'xvc', '', '2019-03-12 17:09:58', '2019-03-12 17:09:58'), 
('7', 'sdf', 'sdf', '2', '2', '1', '1', 'sdf', '', '2019-03-12 17:09:58', '2019-03-12 17:09:58'), 
('8', '1ewds1', '12e', '2', '2', '11', '1', 'ds', '', '2019-03-12 17:09:58', '2019-03-12 17:09:58'), 
('9', '1', 'dcx', '2', '2', '1', '1', 'cx', '', '2019-03-12 17:09:58', '2019-03-12 17:09:58'), 
('10', 'd', 'sdf', '', '12', '12', '121', 'werwer', '', '2019-03-14 05:06:21', '2019-03-14 05:06:21'), 
('11', 'cxv', 'sf', '2', '12', '1', '1212', '0', '', '2019-03-15 11:45:15', '2019-03-15 11:45:15'), 
('12', 'asd', 'asd', '2', '10', '1', '123', 'asd', '', '2019-03-15 11:48:18', '2019-03-15 11:48:18'), 
('13', 'asd', 'asd', '2', '10', '1', '123', 'asd', '', '2019-03-15 11:48:18', '2019-03-15 11:48:18'), 
('14', 'asd', 'asd', '2', '10', '1', '123', 'asd', '', '2019-03-15 11:48:18', '2019-03-15 11:48:18'), 
('15', 'asd', 'asd', '2', '10', '1', '123', 'asd', '', '2019-03-15 11:48:18', '2019-03-15 11:48:18'), 
('16', 'sd', 'zscv', '4', '10', '1', '12', 'zsd', '', '2019-03-15 11:58:38', '2019-03-15 11:58:38'), 
('17', '123', 'lkj', '3', '6', '1', '10000', 'asaaa', '', '2019-04-11 05:27:42', '2019-04-11 05:27:42'), 
('18', '369', 'sadsd', '4', '12', '1', '15242', 'asda', '', '2019-04-11 05:28:17', '2019-04-11 05:28:17'), 
('19', '369', 'sadsd', '4', '12', '1', '15242', 'asda', '', '2019-04-11 05:28:17', '2019-04-11 05:28:17'), 
('20', '12', 'as', '3', '4', '1', '11111', 'sa', '', '2019-04-16 10:53:59', '2019-04-16 10:53:59'), 
('21', '11qw', 'xdv', '3', '6', '1', '11111', 'fvxc', '', '2019-04-16 10:54:00', '2019-04-16 10:54:00'), 
('22', '101', 'gh', '', '6', '1', '12000', 'gy', '', '2019-04-18 10:27:55', '2019-04-18 10:27:55'), 
('23', '111', 'asd', '', '6', '1', '11111', 'asd', '', '2019-04-24 10:50:39', '2019-04-24 10:50:39'), 
('24', '111', 'asd', '', '6', '1', '11111', 'asd', '', '2019-04-24 10:51:38', '2019-04-24 10:51:38'), 
('25', '111', 'asd', '', '6', '1', '11111', 'asd', '', '2019-04-24 10:55:03', '2019-04-24 10:55:03'), 
('26', '111', 'asd', '', '6', '1', '11111', 'asd', '', '2019-04-24 10:57:21', '2019-04-24 10:57:21'), 
('27', '111', 'asd', '', '6', '1', '11111', 'asd', '', '2019-04-24 10:57:36', '2019-04-24 10:57:36'), 
('28', '111', 'asd', '', '6', '1', '11111', 'asd', '', '2019-04-24 10:58:39', '2019-04-24 10:58:39'), 
('29', '111', 'asd', '', '6', '1', '11111', 'asd', '', '2019-04-24 11:00:17', '2019-04-24 11:00:17'), 
('30', '111', 'asd', '', '6', '1', '11111', 'asd', '', '2019-04-24 11:01:09', '2019-04-24 11:01:09'), 
('34', '122', 'fsdf', '', '6', '1', '3000', 'sf', '', '2019-04-25 06:50:54', '2019-04-25 06:50:54'), 
('33', '111', 'oooo', '', '6', '1', '2000', 'hg', '', '2019-04-25 06:50:54', '2019-04-25 06:50:54');  


INSERT INTO `purchase_bill_details` ( `id`, `bill_date`, `bill_no`, `purbillno`, `purbilldate`, `purchaser_id`, `tcgst`, `tsgst`, `discount`, `total`, `amountpay`, `amountpayl`, `created_at`, `updated_at`) VALUES 
('1', '2019-04-18', '10', 'dqw1', '2019-04-18', '2', '360.00', '360.00', '0.00', '12720.00', '12720.00', '0.00', '2019-04-18 10:27:55', '2019-04-18 10:27:55'), 
('2', '2019-04-24', '11', '1q1', '2019-04-24', '3', '333.33', '333.33', '0.00', '11777.66', '11778.00', '0.00', '2019-04-24 10:50:39', '2019-04-24 10:50:39'), 
('3', '2019-04-24', '11', '1q1', '2019-04-24', '3', '333.33', '333.33', '0.00', '11777.66', '11778.00', '0.00', '2019-04-24 10:51:38', '2019-04-24 10:51:38'), 
('4', '2019-04-24', '11', '1q1', '2019-04-24', '3', '333.33', '333.33', '0.00', '11777.66', '11778.00', '0.00', '2019-04-24 10:55:03', '2019-04-24 10:55:03'), 
('5', '2019-04-24', '11', '1q1', '2019-04-24', '3', '333.33', '333.33', '0.00', '11777.66', '11778.00', '0.00', '2019-04-24 10:57:21', '2019-04-24 10:57:21'), 
('6', '2019-04-24', '11', '1q1', '2019-04-24', '3', '333.33', '333.33', '0.00', '11777.66', '11778.00', '0.00', '2019-04-24 10:57:36', '2019-04-24 10:57:36'), 
('7', '2019-04-24', '11', '1q1', '2019-04-24', '3', '333.33', '333.33', '0.00', '11777.66', '11778.00', '0.00', '2019-04-24 10:58:39', '2019-04-24 10:58:39'), 
('8', '2019-04-24', '11', '1q1', '2019-04-24', '3', '333.33', '333.33', '0.00', '11777.66', '11778.00', '0.00', '2019-04-24 11:00:17', '2019-04-24 11:00:17'), 
('9', '2019-04-24', '11', '1q1', '2019-04-24', '3', '333.33', '333.33', '0.00', '11777.66', '11778.00', '0.00', '2019-04-24 11:01:09', '2019-04-24 11:01:09'), 
('11', '2019-04-25', '12', '111', '2019-04-25', '2', '150.00', '150.00', '0.00', '5300.00', '5300.00', '0.00', '2019-04-25 06:50:54', '2019-04-25 06:50:54');  


 


INSERT INTO `purchase_bills` ( `id`, `bill_id`, `bill_no`, `product_code`, `product_name`, `cgstp`, `sgstp`, `product_rate`, `ime_1`, `ime_2`, `cgst`, `sgst`, `quantity`, `total`, `created_at`, `updated_at`) VALUES 
('1', '1', '10', '101', 'gh', '3.00', '3.00', '12000.00', 'gy', '', '360.00', '360.00', '1.00', '12720.00', '2019-04-18 10:27:55', '2019-04-18 10:27:55'), 
('2', '2', '11', '111', 'asd', '3.00', '3.00', '11111.00', 'asd', '', '333.33', '333.33', '1.00', '11777.66', '2019-04-24 10:50:39', '2019-04-24 10:50:39'), 
('3', '3', '11', '111', 'asd', '3.00', '3.00', '11111.00', 'asd', '', '333.33', '333.33', '1.00', '11777.66', '2019-04-24 10:51:38', '2019-04-24 10:51:38'), 
('4', '4', '11', '111', 'asd', '3.00', '3.00', '11111.00', 'asd', '', '333.33', '333.33', '1.00', '11777.66', '2019-04-24 10:55:03', '2019-04-24 10:55:03'), 
('5', '5', '11', '111', 'asd', '3.00', '3.00', '11111.00', 'asd', '', '333.33', '333.33', '1.00', '11777.66', '2019-04-24 10:57:21', '2019-04-24 10:57:21'), 
('6', '6', '11', '111', 'asd', '3.00', '3.00', '11111.00', 'asd', '', '333.33', '333.33', '1.00', '11777.66', '2019-04-24 10:57:36', '2019-04-24 10:57:36'), 
('7', '7', '11', '111', 'asd', '3.00', '3.00', '11111.00', 'asd', '', '333.33', '333.33', '1.00', '11777.66', '2019-04-24 10:58:39', '2019-04-24 10:58:39'), 
('8', '8', '11', '111', 'asd', '3.00', '3.00', '11111.00', 'asd', '', '333.33', '333.33', '1.00', '11777.66', '2019-04-24 11:00:17', '2019-04-24 11:00:17'), 
('9', '9', '11', '111', 'asd', '3.00', '3.00', '11111.00', 'asd', '', '333.33', '333.33', '1.00', '11777.66', '2019-04-24 11:01:09', '2019-04-24 11:01:09'), 
('13', '11', '12', '122', 'fsdf', '3.00', '3.00', '3000.00', 'sf', '', '90.00', '90.00', '1.00', '3180.00', '2019-04-25 06:50:54', '2019-04-25 06:50:54'), 
('12', '11', '12', '111', 'oooo', '3.00', '3.00', '2000.00', 'hg', '', '60.00', '60.00', '1.00', '2120.00', '2019-04-25 06:50:54', '2019-04-25 06:50:54');  


INSERT INTO `purchase_payments` ( `id`, `billno`, `desc`, `billdate`, `mode`, `ref`, `amount`, `created_at`, `updated_at`) VALUES 
('11', '10', 'Bill', '2019-04-18', 'cash', 'cash', '12720.00', '2019-04-18 10:27:55', '2019-04-18 10:27:55'), 
('15', '12', 'Bill', '2019-04-25', 'cash', 'cash', '5300.00', '2019-04-25 06:50:54', '2019-04-25 06:50:54'), 
('14', '11', 'Bill', '2019-04-24', 'cash', 'cash', '11778.00', '2019-04-24 11:01:09', '2019-04-24 11:01:09');  


INSERT INTO `purchasers` ( `id`, `name`, `phone`, `amount`, `gst`, `created_at`, `updated_at`) VALUES 
('2', 'Nitin', '6985741231', '20.00', 'sdrgsdrx', '2019-03-11 05:36:58', '2019-03-11 05:36:58'), 
('3', 'sfd', '9847296004', '-11355.68', 'rdsf', '2019-03-12 04:39:54', '2019-03-12 04:39:54'), 
('4', 'sdf', '9847296004', '526.08', 'werwe', '2019-03-15 11:56:45', '2019-03-15 11:56:45');  


INSERT INTO `sales_bill_details` ( `id`, `bill_date`, `bill_no`, `customer_id`, `tcgst`, `tsgst`, `total`, `amountpay`, `amountpayl`, `created_at`, `updated_at`) VALUES 
('1', '2019-04-18', '11', '2', '450.00', '450.00', '15000.00', '15000.00', '0.00', '2019-04-18 08:45:48', '2019-04-18 08:45:48'), 
('2', '2019-04-18', '12', '2', '1050.00', '1050.00', '35000.00', '35000.00', '0.00', '2019-04-18 10:23:04', '2019-04-18 10:23:04'), 
('3', '2019-04-20', '13', '2', '450.00', '450.00', '15000.00', '15000.00', '0.00', '2019-04-20 08:01:55', '2019-04-20 08:01:55'), 
('4', '2019-04-20', '14', '6', '600.00', '600.00', '20000.00', '20000.00', '0.00', '2019-04-20 08:05:20', '2019-04-20 08:05:20');  


 


INSERT INTO `sales_bills` ( `id`, `bill_id`, `bill_no`, `product_id`, `rate`, `quantity`, `total`, `created_at`, `updated_at`) VALUES 
('1', '1', '11', '1', '14100.00', '1.00', '15000', '2019-04-18 08:45:48', '2019-04-18 08:45:48'), 
('2', '2', '12', '1', '18800.00', '1.00', '20000', '2019-04-18 10:23:04', '2019-04-18 10:23:04'), 
('3', '2', '12', '2', '14100.00', '1.00', '15000', '2019-04-18 10:23:04', '2019-04-18 10:23:04'), 
('4', '3', '13', '1', '14100.00', '1.00', '15000', '2019-04-20 08:01:55', '2019-04-20 08:01:55'), 
('5', '4', '14', '2', '18800.00', '1.00', '20000', '2019-04-20 08:05:20', '2019-04-20 08:05:20');  


 


INSERT INTO `sales_payments` ( `id`, `billno`, `desc`, `billdate`, `mode`, `ref`, `amount`, `created_at`, `updated_at`) VALUES 
('9', '11', 'Bill', '2019-04-18', 'cash', 'cash', '15000.00', '2019-04-18 08:45:48', '2019-04-18 08:45:48'), 
('10', '12', 'Bill', '2019-04-18', 'cash', 'cash', '35000.00', '2019-04-18 10:23:04', '2019-04-18 10:23:04'), 
('14', '14', 'Bill', '2019-04-20', 'bank', '1234', '20000.00', '2019-04-20 08:05:20', '2019-04-20 08:05:20'), 
('13', '13', 'Bill', '2019-04-20', 'cash', 'cash', '15000.00', '2019-04-20 08:01:55', '2019-04-20 08:01:55');  


INSERT INTO `users` ( `id`, `name`, `username`, `role`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES 
('1', 'Admin', 'admin', '0', '$2y$10$HjlwUNi/s8bpXh09PCZ68OIQAnD4h4ykpEQACDw0WrEIW3tFZll1W', '', '2019-02-27 11:04:45', '2019-02-27 11:04:45'); 