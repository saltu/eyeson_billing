-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 21, 2019 at 08:09 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eyeson`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
CREATE TABLE IF NOT EXISTS `accounts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `name`, `amount`, `created_at`, `updated_at`) VALUES
(1, 'Account', 263788, NULL, NULL),
(2, 'Sales', 366549, NULL, NULL),
(3, 'Purchase', 103084, NULL, NULL),
(4, 'Expense', 421, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `phone`, `amount`, `created_at`, `updated_at`) VALUES
(1, 'dxc', '9847296004', NULL, '2019-03-08 00:36:53', '2019-03-08 00:36:53'),
(2, 'NIthn', '9847296004', NULL, '2019-03-08 00:38:33', '2019-03-08 00:38:33'),
(6, 'sd', '9847296004', NULL, '2019-03-11 23:21:11', '2019-03-11 23:21:11'),
(7, 'akhil', '1234567895', 0.2, '2019-08-02 00:48:35', '2019-08-02 00:48:35');

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

DROP TABLE IF EXISTS `expenses`;
CREATE TABLE IF NOT EXISTS `expenses` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `ex_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`id`, `description`, `amount`, `ex_date`, `created_at`, `updated_at`) VALUES
(1, 'Tea', 100, '2019-03-06', '2019-03-06 06:31:57', '2019-03-06 06:31:57'),
(2, 'zx', 12, '2019-03-12', '2019-03-12 11:34:25', '2019-03-12 11:34:25'),
(3, 'scx', 121, '2019-03-12', '2019-03-12 11:35:30', '2019-03-12 11:35:30'),
(4, 'Tea', 100, '2019-04-11', '2019-04-10 23:58:39', '2019-04-10 23:58:39'),
(5, 'paper', 200, '2019-04-11', '2019-04-10 23:58:50', '2019-04-10 23:58:50');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_nums`
--

DROP TABLE IF EXISTS `invoice_nums`;
CREATE TABLE IF NOT EXISTS `invoice_nums` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice_num` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoice_nums`
--

INSERT INTO `invoice_nums` (`id`, `name`, `invoice_num`, `created_at`, `updated_at`) VALUES
(1, 'Sales', 18, NULL, NULL),
(2, 'Purchase', 13, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_03_04_053630_create_products_table', 2),
(4, '2019_03_04_070900_create_expenses_table', 3),
(5, '2019_03_04_102935_create_invoice_nums_table', 4),
(19, '2019_04_18_072235_create_sales_bill_details_table', 10),
(7, '2019_03_08_053700_create_customers_table', 5),
(8, '2019_03_08_053806_create_sales_bill_temps_table', 5),
(9, '2019_03_09_053954_create_sales_payments_table', 6),
(10, '2019_03_04_070954_create_accounts_table', 7),
(11, '2019_03_11_040031_create_purchase_payments_table', 8),
(20, '2019_03_11_040048_create_purchase_bills_table', 11),
(13, '2019_03_11_040057_create_purchase_bill_temps_table', 8),
(14, '2019_03_11_040118_create_purchasers_table', 8),
(15, '2019_03_04_071041_create_open_closes_table', 9),
(18, '2019_03_04_070830_create_sales_bills_table', 10),
(21, '2019_04_18_085048_create_purchase_bill_details_table', 11),
(22, '2019_04_23_054853_create_sales_estimates_table', 12);

-- --------------------------------------------------------

--
-- Table structure for table `open_closes`
--

DROP TABLE IF EXISTS `open_closes`;
CREATE TABLE IF NOT EXISTS `open_closes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `current_date` date DEFAULT NULL,
  `opening` double DEFAULT NULL,
  `closing` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `open_closes`
--

INSERT INTO `open_closes` (`id`, `current_date`, `opening`, `closing`, `created_at`, `updated_at`) VALUES
(1, '2019-03-12', 56914, NULL, '2019-03-25 01:38:51', '2019-03-25 01:38:51'),
(2, '2019-03-28', 56914, NULL, '2019-03-27 23:34:54', '2019-03-27 23:34:54'),
(3, '2019-03-30', 56914, NULL, '2019-03-30 00:54:31', '2019-03-30 00:54:31'),
(4, '2019-04-02', 56914, NULL, '2019-04-01 23:54:13', '2019-04-01 23:54:13'),
(5, '2019-04-03', 56914, NULL, '2019-04-02 23:19:27', '2019-04-02 23:19:27'),
(6, '2019-04-04', 56914, NULL, '2019-04-03 23:16:44', '2019-04-03 23:16:44'),
(7, '2019-04-05', 56914, NULL, '2019-04-04 23:27:38', '2019-04-04 23:27:38'),
(8, '2019-04-06', 56914, NULL, '2019-04-06 02:20:43', '2019-04-06 02:20:43'),
(9, '2019-04-11', 56914, NULL, '2019-04-10 23:41:40', '2019-04-10 23:41:40'),
(10, '2019-04-12', 96730, NULL, '2019-04-11 23:54:37', '2019-04-11 23:54:37'),
(11, '2019-04-16', 96730, NULL, '2019-04-16 00:12:28', '2019-04-16 00:12:28'),
(12, '2019-04-17', 73597, NULL, '2019-04-16 23:18:35', '2019-04-16 23:18:35'),
(13, '2019-04-18', 73597, NULL, '2019-04-18 00:28:10', '2019-04-18 00:28:10'),
(14, '2019-04-20', 181117, NULL, '2019-04-19 23:17:42', '2019-04-19 23:17:42'),
(15, '2019-04-22', 216117, NULL, '2019-04-21 23:27:49', '2019-04-21 23:27:49'),
(16, '2019-04-23', 216117, NULL, '2019-04-22 23:00:55', '2019-04-22 23:00:55'),
(17, '2019-04-24', 216117, NULL, '2019-04-23 23:27:48', '2019-04-23 23:27:48'),
(18, '2019-04-25', 204339, NULL, '2019-04-25 01:10:12', '2019-04-25 01:10:12'),
(19, '2019-04-26', 199039, NULL, '2019-04-26 00:02:03', '2019-04-26 00:02:03'),
(20, '2019-04-27', 199039, NULL, '2019-04-26 23:25:59', '2019-04-26 23:25:59'),
(21, '2019-04-29', 199039, NULL, '2019-04-29 02:51:29', '2019-04-29 02:51:29'),
(22, '2019-04-30', 199039, NULL, '2019-04-29 23:20:46', '2019-04-29 23:20:46'),
(23, '2019-05-01', 199039, NULL, '2019-04-30 23:24:45', '2019-04-30 23:24:45'),
(24, '2019-05-02', 204039, NULL, '2019-05-01 23:52:20', '2019-05-01 23:52:20'),
(25, '2019-05-07', 204039, NULL, '2019-05-07 02:08:45', '2019-05-07 02:08:45'),
(26, '2019-05-16', 204039, NULL, '2019-05-16 02:56:04', '2019-05-16 02:56:04'),
(27, '2019-07-31', 204039, NULL, '2019-07-31 01:39:07', '2019-07-31 01:39:07'),
(28, '2019-08-02', 204039, NULL, '2019-08-01 23:51:34', '2019-08-01 23:51:34'),
(29, '2019-08-03', 204039, NULL, '2019-08-02 23:56:21', '2019-08-02 23:56:21'),
(30, '2019-08-31', 263788, NULL, '2019-08-31 00:14:39', '2019-08-31 00:14:39'),
(31, '2019-09-02', 263788, NULL, '2019-09-02 00:22:36', '2019-09-02 00:22:36'),
(32, '2019-09-04', 263788, NULL, '2019-09-04 00:24:57', '2019-09-04 00:24:57'),
(33, '2019-09-06', 263788, NULL, '2019-09-05 23:24:39', '2019-09-05 23:24:39'),
(34, '2019-09-07', 263788, NULL, '2019-09-06 23:47:32', '2019-09-06 23:47:32'),
(35, '2019-10-18', 263788, NULL, '2019-10-17 23:54:28', '2019-10-17 23:54:28'),
(36, '2019-11-16', 263788, NULL, '2019-11-16 02:17:52', '2019-11-16 02:17:52'),
(37, '2019-11-21', 263788, NULL, '2019-11-21 04:20:12', '2019-11-21 04:20:12');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchased` int(11) DEFAULT NULL,
  `gst` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `ime_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ime_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `code`, `name`, `purchased`, `gst`, `quantity`, `rate`, `ime_1`, `ime_2`, `created_at`, `updated_at`) VALUES
(1, '123', 'iphone', 2, 6, 5, 11555, '12345678901111111111', '', '2019-03-09 01:37:05', '2019-03-09 01:37:05'),
(2, '321', 'oppo', 2, 6, 4, 10000, '9876543210', '', '2019-03-09 01:37:29', '2019-03-09 01:37:29'),
(3, 'sd', 'sd', 2, 2, 1, 1, 'sd', NULL, '2019-03-11 03:10:44', '2019-03-11 03:10:44'),
(4, 'aaa', 'aa', 2, 12, 1, 1000, 'aa', NULL, '2019-03-11 04:47:57', '2019-03-11 04:47:57'),
(5, 'sdf', 'sdf', 2, 12, 1, 1111, 'sdf', NULL, '2019-03-12 11:39:58', '2019-03-12 11:39:58'),
(6, 'sdfx', 'sdf', 2, 12, 1, 12, 'xvc', NULL, '2019-03-12 11:39:58', '2019-03-12 11:39:58'),
(7, 'sdf', 'sdf', 2, 2, 1, 1, 'sdf', NULL, '2019-03-12 11:39:58', '2019-03-12 11:39:58'),
(8, '1ewds1', '12e', 2, 2, 11, 1, 'ds', NULL, '2019-03-12 11:39:58', '2019-03-12 11:39:58'),
(9, '1', 'dcx', 2, 2, 1, 1, 'cx', NULL, '2019-03-12 11:39:58', '2019-03-12 11:39:58'),
(10, 'd', 'sdf', NULL, 12, 12, 121, 'werwer', NULL, '2019-03-13 23:36:21', '2019-03-13 23:36:21'),
(11, 'cxv', 'sf', 2, 12, 1, 1212, '0', '', '2019-03-15 06:15:15', '2019-03-15 06:15:15'),
(12, 'asd', 'asd', 2, 10, 1, 123, 'asd', NULL, '2019-03-15 06:18:18', '2019-03-15 06:18:18'),
(13, 'asd', 'asd', 2, 10, 1, 123, 'asd', NULL, '2019-03-15 06:18:18', '2019-03-15 06:18:18'),
(14, 'asd', 'asd', 2, 10, 1, 123, 'asd', NULL, '2019-03-15 06:18:18', '2019-03-15 06:18:18'),
(15, 'asd', 'asd', 2, 10, 1, 123, 'asd', NULL, '2019-03-15 06:18:18', '2019-03-15 06:18:18'),
(16, 'sd', 'zscv', 4, 10, 1, 12, 'zsd', NULL, '2019-03-15 06:28:38', '2019-03-15 06:28:38'),
(17, '123', 'lkj', 3, 6, 1, 10000, 'asaaa', NULL, '2019-04-10 23:57:42', '2019-04-10 23:57:42'),
(18, '369', 'sadsd', 4, 12, 1, 15242, 'asda', NULL, '2019-04-10 23:58:17', '2019-04-10 23:58:17'),
(19, '369', 'sadsd', 4, 12, 1, 15242, 'asda', NULL, '2019-04-10 23:58:17', '2019-04-10 23:58:17'),
(20, '12', 'as', 3, 4, 1, 11111, 'sa', NULL, '2019-04-16 05:23:59', '2019-04-16 05:23:59'),
(21, '11qw', 'xdv', 3, 6, 1, 11111, 'fvxc', NULL, '2019-04-16 05:24:00', '2019-04-16 05:24:00'),
(22, '101', 'gh', NULL, 6, 1, 12000, 'gy', NULL, '2019-04-18 04:57:55', '2019-04-18 04:57:55'),
(23, '111', 'asd', NULL, 6, 1, 11111, 'asd', NULL, '2019-04-24 05:20:39', '2019-04-24 05:20:39'),
(24, '111', 'asd', NULL, 6, 1, 11111, 'asd', NULL, '2019-04-24 05:21:38', '2019-04-24 05:21:38'),
(25, '111', 'asd', NULL, 6, 1, 11111, 'asd', NULL, '2019-04-24 05:25:03', '2019-04-24 05:25:03'),
(26, '111', 'asd', NULL, 6, 1, 11111, 'asd', NULL, '2019-04-24 05:27:21', '2019-04-24 05:27:21'),
(27, '111', 'asd', NULL, 6, 1, 11111, 'asd', NULL, '2019-04-24 05:27:36', '2019-04-24 05:27:36'),
(28, '111', 'asd', NULL, 6, 1, 11111, 'asd', NULL, '2019-04-24 05:28:39', '2019-04-24 05:28:39'),
(29, '111', 'asd', NULL, 6, 1, 11111, 'asd', NULL, '2019-04-24 05:30:17', '2019-04-24 05:30:17'),
(30, '111', 'asd', NULL, 6, 1, 11111, 'asd', NULL, '2019-04-24 05:31:09', '2019-04-24 05:31:09'),
(34, '122', 'fsdf', NULL, 6, 1, 3000, 'sf', NULL, '2019-04-25 01:20:54', '2019-04-25 01:20:54'),
(33, '111', 'oooo', NULL, 6, 0, 2000, 'hg', NULL, '2019-04-25 01:20:54', '2019-04-25 01:20:54'),
(35, '111', 'as', NULL, 1, 1, 111, 'ww1111111', NULL, '2019-05-16 03:26:49', '2019-05-16 03:26:49');

-- --------------------------------------------------------

--
-- Table structure for table `purchasers`
--

DROP TABLE IF EXISTS `purchasers`;
CREATE TABLE IF NOT EXISTS `purchasers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(8,2) DEFAULT NULL,
  `gst` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchasers`
--

INSERT INTO `purchasers` (`id`, `name`, `phone`, `amount`, `gst`, `created_at`, `updated_at`) VALUES
(2, 'Nitin', '6985741231', 20.00, 'sdrgsdrx', '2019-03-11 00:06:58', '2019-03-11 00:06:58'),
(3, 'sfd', '9847296004', -11355.68, 'rdsf', '2019-03-11 23:09:54', '2019-03-11 23:09:54'),
(4, 'sdf', '9847296004', 526.08, 'werwe', '2019-03-15 06:26:45', '2019-03-15 06:26:45');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_bills`
--

DROP TABLE IF EXISTS `purchase_bills`;
CREATE TABLE IF NOT EXISTS `purchase_bills` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bill_id` int(11) DEFAULT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgstp` double(8,2) DEFAULT NULL,
  `sgstp` double(8,2) DEFAULT NULL,
  `product_rate` double(8,2) DEFAULT NULL,
  `ime_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ime_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgst` double(8,2) DEFAULT NULL,
  `sgst` double(8,2) DEFAULT NULL,
  `quantity` double(8,2) DEFAULT NULL,
  `total` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchase_bills`
--

INSERT INTO `purchase_bills` (`id`, `bill_id`, `bill_no`, `product_code`, `product_name`, `cgstp`, `sgstp`, `product_rate`, `ime_1`, `ime_2`, `cgst`, `sgst`, `quantity`, `total`, `created_at`, `updated_at`) VALUES
(1, 1, '10', '101', 'gh', 3.00, 3.00, 12000.00, 'gy', NULL, 360.00, 360.00, 1.00, 12720.00, '2019-04-18 04:57:55', '2019-04-18 04:57:55'),
(2, 2, '11', '111', 'asd', 3.00, 3.00, 11111.00, 'asd', NULL, 333.33, 333.33, 1.00, 11777.66, '2019-04-24 05:20:39', '2019-04-24 05:20:39'),
(3, 3, '11', '111', 'asd', 3.00, 3.00, 11111.00, 'asd', NULL, 333.33, 333.33, 1.00, 11777.66, '2019-04-24 05:21:38', '2019-04-24 05:21:38'),
(4, 4, '11', '111', 'asd', 3.00, 3.00, 11111.00, 'asd', NULL, 333.33, 333.33, 1.00, 11777.66, '2019-04-24 05:25:03', '2019-04-24 05:25:03'),
(5, 5, '11', '111', 'asd', 3.00, 3.00, 11111.00, 'asd', NULL, 333.33, 333.33, 1.00, 11777.66, '2019-04-24 05:27:21', '2019-04-24 05:27:21'),
(6, 6, '11', '111', 'asd', 3.00, 3.00, 11111.00, 'asd', NULL, 333.33, 333.33, 1.00, 11777.66, '2019-04-24 05:27:36', '2019-04-24 05:27:36'),
(7, 7, '11', '111', 'asd', 3.00, 3.00, 11111.00, 'asd', NULL, 333.33, 333.33, 1.00, 11777.66, '2019-04-24 05:28:39', '2019-04-24 05:28:39'),
(8, 8, '11', '111', 'asd', 3.00, 3.00, 11111.00, 'asd', NULL, 333.33, 333.33, 1.00, 11777.66, '2019-04-24 05:30:17', '2019-04-24 05:30:17'),
(9, 9, '11', '111', 'asd', 3.00, 3.00, 11111.00, 'asd', NULL, 333.33, 333.33, 1.00, 11777.66, '2019-04-24 05:31:09', '2019-04-24 05:31:09'),
(13, 11, '12', '122', 'fsdf', 3.00, 3.00, 3000.00, 'sf', NULL, 90.00, 90.00, 1.00, 3180.00, '2019-04-25 01:20:54', '2019-04-25 01:20:54'),
(12, 11, '12', '111', 'oooo', 3.00, 3.00, 2000.00, 'hg', NULL, 60.00, 60.00, 1.00, 2120.00, '2019-04-25 01:20:54', '2019-04-25 01:20:54');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_bill_details`
--

DROP TABLE IF EXISTS `purchase_bill_details`;
CREATE TABLE IF NOT EXISTS `purchase_bill_details` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bill_date` date DEFAULT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purbillno` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purbilldate` date DEFAULT NULL,
  `purchaser_id` int(11) DEFAULT NULL,
  `tcgst` double(8,2) DEFAULT NULL,
  `tsgst` double(8,2) DEFAULT NULL,
  `discount` double(8,2) DEFAULT NULL,
  `total` double(8,2) DEFAULT NULL,
  `amountpay` double(8,2) DEFAULT NULL,
  `amountpayl` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchase_bill_details`
--

INSERT INTO `purchase_bill_details` (`id`, `bill_date`, `bill_no`, `purbillno`, `purbilldate`, `purchaser_id`, `tcgst`, `tsgst`, `discount`, `total`, `amountpay`, `amountpayl`, `created_at`, `updated_at`) VALUES
(1, '2019-04-18', '10', 'dqw1', '2019-04-18', 2, 360.00, 360.00, 0.00, 12720.00, 12720.00, 0.00, '2019-04-18 04:57:55', '2019-04-18 04:57:55'),
(2, '2019-04-24', '11', '1q1', '2019-04-24', 3, 333.33, 333.33, 0.00, 11777.66, 11778.00, 0.00, '2019-04-24 05:20:39', '2019-04-24 05:20:39'),
(3, '2019-04-24', '11', '1q1', '2019-04-24', 3, 333.33, 333.33, 0.00, 11777.66, 11778.00, 0.00, '2019-04-24 05:21:38', '2019-04-24 05:21:38'),
(4, '2019-04-24', '11', '1q1', '2019-04-24', 3, 333.33, 333.33, 0.00, 11777.66, 11778.00, 0.00, '2019-04-24 05:25:03', '2019-04-24 05:25:03'),
(5, '2019-04-24', '11', '1q1', '2019-04-24', 3, 333.33, 333.33, 0.00, 11777.66, 11778.00, 0.00, '2019-04-24 05:27:21', '2019-04-24 05:27:21'),
(6, '2019-04-24', '11', '1q1', '2019-04-24', 3, 333.33, 333.33, 0.00, 11777.66, 11778.00, 0.00, '2019-04-24 05:27:36', '2019-04-24 05:27:36'),
(7, '2019-04-24', '11', '1q1', '2019-04-24', 3, 333.33, 333.33, 0.00, 11777.66, 11778.00, 0.00, '2019-04-24 05:28:39', '2019-04-24 05:28:39'),
(8, '2019-04-24', '11', '1q1', '2019-04-24', 3, 333.33, 333.33, 0.00, 11777.66, 11778.00, 0.00, '2019-04-24 05:30:17', '2019-04-24 05:30:17'),
(9, '2019-04-24', '11', '1q1', '2019-04-24', 3, 333.33, 333.33, 0.00, 11777.66, 11778.00, 0.00, '2019-04-24 05:31:09', '2019-04-24 05:31:09'),
(11, '2019-04-25', '12', '111', '2019-04-25', 2, 150.00, 150.00, 0.00, 5300.00, 5300.00, 0.00, '2019-04-25 01:20:54', '2019-04-25 01:20:54');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_bill_temps`
--

DROP TABLE IF EXISTS `purchase_bill_temps`;
CREATE TABLE IF NOT EXISTS `purchase_bill_temps` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bill_date` date DEFAULT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purbillno` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purbilldate` date DEFAULT NULL,
  `purchaser_id` int(11) DEFAULT NULL,
  `product_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgstp` float DEFAULT NULL,
  `sgstp` float DEFAULT NULL,
  `product_rate` float DEFAULT NULL,
  `ime_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ime_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cgst` double(8,2) DEFAULT NULL,
  `sgst` double(8,2) DEFAULT NULL,
  `quantity` double(8,2) DEFAULT NULL,
  `discount` double(8,2) DEFAULT NULL,
  `total` double(8,2) DEFAULT NULL,
  `amountpay` double(8,2) DEFAULT NULL,
  `amountpayl` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchase_bill_temps`
--

INSERT INTO `purchase_bill_temps` (`id`, `bill_date`, `bill_no`, `purbillno`, `purbilldate`, `purchaser_id`, `product_code`, `product_name`, `cgstp`, `sgstp`, `product_rate`, `ime_1`, `ime_2`, `cgst`, `sgst`, `quantity`, `discount`, `total`, `amountpay`, `amountpayl`, `created_at`, `updated_at`) VALUES
(1, '2019-05-01', '13', '121', '2019-05-01', 3, '111', 'isds', 3, 3, 10200, 'sd', NULL, 306.00, 306.00, 1.00, 0.00, 10812.00, NULL, NULL, '2019-04-30 23:52:06', '2019-04-30 23:52:06'),
(2, '2019-05-01', '13', '121', '2019-05-01', 3, '125', 'ijgjgb', 3, 3, 5000, '24452153564156', NULL, 750.00, 750.00, 5.00, 0.00, 26500.00, NULL, NULL, '2019-08-03 02:29:03', '2019-08-03 02:29:03');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_payments`
--

DROP TABLE IF EXISTS `purchase_payments`;
CREATE TABLE IF NOT EXISTS `purchase_payments` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `billno` int(11) DEFAULT NULL,
  `desc` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billdate` date DEFAULT NULL,
  `mode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `purchase_payments`
--

INSERT INTO `purchase_payments` (`id`, `billno`, `desc`, `billdate`, `mode`, `ref`, `amount`, `created_at`, `updated_at`) VALUES
(11, 10, 'Bill', '2019-04-18', 'cash', 'cash', 12720.00, '2019-04-18 04:57:55', '2019-04-18 04:57:55'),
(15, 12, 'Bill', '2019-04-25', 'cash', 'cash', 5300.00, '2019-04-25 01:20:54', '2019-04-25 01:20:54'),
(14, 11, 'Bill', '2019-04-24', 'cash', 'cash', 11778.00, '2019-04-24 05:31:09', '2019-04-24 05:31:09');

-- --------------------------------------------------------

--
-- Table structure for table `sales_bills`
--

DROP TABLE IF EXISTS `sales_bills`;
CREATE TABLE IF NOT EXISTS `sales_bills` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bill_id` int(11) DEFAULT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales_bills`
--

INSERT INTO `sales_bills` (`id`, `bill_id`, `bill_no`, `product_id`, `rate`, `quantity`, `total`, `created_at`, `updated_at`) VALUES
(1, 1, '11', 1, 14100, 1, 15000, '2019-04-18 03:15:48', '2019-04-18 03:15:48'),
(2, 2, '12', 1, 18800, 1, 20000, '2019-04-18 04:53:04', '2019-04-18 04:53:04'),
(3, 2, '12', 2, 14100, 1, 15000, '2019-04-18 04:53:04', '2019-04-18 04:53:04'),
(4, 3, '13', 1, 14100, 1, 15000, '2019-04-20 02:31:55', '2019-04-20 02:31:55'),
(5, 4, '14', 2, 18800, 1, 20000, '2019-04-20 02:35:20', '2019-04-20 02:35:20'),
(6, 5, '15', 2, 9951, 1, 10700, '2019-08-03 00:11:49', '2019-08-03 00:11:49'),
(7, 5, '15', 33, 2325, 1, 2500, '2019-08-03 00:11:49', '2019-08-03 00:11:49'),
(8, 6, '16', 1, 12090, 1, 13000, '2019-08-03 00:40:02', '2019-08-03 00:40:02'),
(9, 6, '16', 2, 9951, 1, 10700, '2019-08-03 00:40:02', '2019-08-03 00:40:02'),
(10, 7, '17', 2, 9858.93, 1, 10601, '2019-08-03 01:35:13', '2019-08-03 01:35:13'),
(11, 7, '17', 1, 11390.8, 1, 12248.2, '2019-08-03 01:35:13', '2019-08-03 01:35:13');

-- --------------------------------------------------------

--
-- Table structure for table `sales_bill_details`
--

DROP TABLE IF EXISTS `sales_bill_details`;
CREATE TABLE IF NOT EXISTS `sales_bill_details` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bill_date` date DEFAULT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `tcgst` float DEFAULT NULL,
  `tsgst` float DEFAULT NULL,
  `tcess` float NOT NULL,
  `total` float DEFAULT NULL,
  `amountpay` float DEFAULT NULL,
  `amountpayl` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales_bill_details`
--

INSERT INTO `sales_bill_details` (`id`, `bill_date`, `bill_no`, `customer_id`, `tcgst`, `tsgst`, `tcess`, `total`, `amountpay`, `amountpayl`, `created_at`, `updated_at`) VALUES
(1, '2019-04-18', '11', 2, 450, 450, 0, 15000, 10000, 0, '2019-04-18 03:15:48', '2019-04-18 03:15:48'),
(2, '2019-04-18', '12', 2, 1050, 1050, 0, 35000, 35000, 0, '2019-04-18 04:53:04', '2019-04-18 04:53:04'),
(3, '2019-04-20', '13', 2, 450, 450, 0, 15000, 15000, 0, '2019-04-20 02:31:55', '2019-04-20 02:31:55'),
(4, '2019-04-20', '14', 6, 600, 600, 0, 20000, 20000, 0, '2019-04-20 02:35:20', '2019-04-20 02:35:20'),
(5, '2019-08-02', '15', 7, 396, 396, 0, 13200, 13200, 0, '2019-08-03 00:11:49', '2019-08-03 00:11:49'),
(6, '2019-08-03', '16', 7, 711, 711, 237, 23700, 23700, 0, '2019-08-03 00:40:02', '2019-08-03 00:40:02'),
(7, '2019-08-03', '17', 7, 685.476, 685.476, 228.492, 22849.2, 22849, 0, '2019-08-03 01:35:13', '2019-08-03 01:35:13');

-- --------------------------------------------------------

--
-- Table structure for table `sales_bill_temps`
--

DROP TABLE IF EXISTS `sales_bill_temps`;
CREATE TABLE IF NOT EXISTS `sales_bill_temps` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bill_date` date DEFAULT NULL,
  `bill_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `cgst` float DEFAULT NULL,
  `sgst` float DEFAULT NULL,
  `cess` float NOT NULL,
  `quantity` float DEFAULT NULL,
  `total` float DEFAULT NULL,
  `amountpay` float DEFAULT NULL,
  `amountpayl` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sales_estimates`
--

DROP TABLE IF EXISTS `sales_estimates`;
CREATE TABLE IF NOT EXISTS `sales_estimates` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bill_date` date DEFAULT NULL,
  `customer_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `rate` double(8,2) DEFAULT NULL,
  `quantity` double(8,2) DEFAULT NULL,
  `total` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sales_payments`
--

DROP TABLE IF EXISTS `sales_payments`;
CREATE TABLE IF NOT EXISTS `sales_payments` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `billno` int(11) DEFAULT NULL,
  `desc` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billdate` date DEFAULT NULL,
  `mode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales_payments`
--

INSERT INTO `sales_payments` (`id`, `billno`, `desc`, `billdate`, `mode`, `ref`, `amount`, `created_at`, `updated_at`) VALUES
(9, 11, 'Bill', '2019-04-18', 'cash', 'cash', 15000.00, '2019-04-18 03:15:48', '2019-04-18 03:15:48'),
(10, 12, 'Bill', '2019-04-18', 'cash', 'cash', 35000.00, '2019-04-18 04:53:04', '2019-04-18 04:53:04'),
(14, 14, 'Bill', '2019-04-20', 'bank', '1234', 20000.00, '2019-04-20 02:35:20', '2019-04-20 02:35:20'),
(13, 13, 'Bill', '2019-04-20', 'cash', 'cash', 15000.00, '2019-04-20 02:31:55', '2019-04-20 02:31:55'),
(16, 15, 'Bill', '2019-08-02', 'cash', 'cash', 13200.00, '2019-08-03 00:11:49', '2019-08-03 00:11:49'),
(17, 16, 'Bill', '2019-08-03', 'cash', 'cash', 23700.00, '2019-08-03 00:40:02', '2019-08-03 00:40:02'),
(18, 17, 'Bill', '2019-08-03', 'cash', 'cash', 22849.00, '2019-08-03 01:35:13', '2019-08-03 01:35:13');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `role`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', 0, '$2y$10$HjlwUNi/s8bpXh09PCZ68OIQAnD4h4ykpEQACDw0WrEIW3tFZll1W', NULL, '2019-02-27 05:34:45', '2019-02-27 05:34:45');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
